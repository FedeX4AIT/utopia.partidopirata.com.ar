---
title: Salarios por Facebook
author: Laurel Ptak
layout: post
signature: 0
toc: false
cover: assets/covers/salarios_por_fb_tapa.png
contra: assets/covers/salarios_por_fb_contra.png
---

Salarios por Facebook
=====================

> Esta es una traducción pirata de "[Wages for
> Facebook](http://wagesforfacebook.com/)", manifiesto escrito por
> Laurel Ptak en 2014.

Eso que llaman amistad es trabajo no remunerado.  Con cada me gusta,
mensaje, etiqueta o click nuestras subjetividades les generan ganancias.
Eso que llaman compartir para nosotres es robo.  Hemos estado atades a
sus términos de servicio demasiado tiempo, es hora de imponer los
nuestros.

Demandar salarios por Facebook es visibilizar que todas nuestras
opiniones y emociones han sido deformadas hacia una función específica
de nuestras vidas conectadas y devuelta como un modelo en el que debemos
caber si queremos ser aceptades en esta sociedad.  Las puntas de
nuestros dedos están deformados de tanto _laikear_ cosas, nuestros
sentimientos se han perdido entre tantas amistades.

El capital tuvo que convencernos de que todo esto es una actividad
natural, inevitable e incluso satisfactoria para que aceptemos el
trabajo no remunerado que realmente es.  A su vez, la condición no
asalariada de Facebook ha sido el arma más poderosa para reforzar en el
sentido común la idea de que usar Facebook no es trabajar y por lo tanto
previene que luchemos encontra de eso.  Somos vistes como usuaries o
amigues en potencia, no trabajadoras en lucha.  Debemos admitir que el
capital ha tenido éxito en ocultar nuestro trabajo.

Al negarnos salarios por el tiempo que pasamos en Facebook y obtener
ganancias directamente de datos camuflados en actos de amistad, el
capital ha logrado varios objetivos a la vez.  En  primer lugar, ha
capturado un montón de trabajo por nada y se ha asegurado que nosotres,
en lugar de luchar en contra, lo busquemos como lo mejor que podemos
hacer en línea.

Las dificultades y ambigüedades en discutir salarios por Facebook
aparecen si los reducimos a una suma de dinero en lugar de una
perspectiva política.  La diferencia entre ambos es enorme.  Proponer
remuneración salarial por Facebook como una cosa en lugar de como
perspectiva, es separar el objetivo final de nuestras luchas de las
luchas mismas e ignorar su importancia en la demistificación y
subversión del rol al que nos han confinado en la sociedad capitalista.

Si tomamos los salarios por Facebook como una perspectiva política,
podemos tomar las luchas por ellos como una forma de provocar una
revolución en nuestras vidas y en nuestro poder social.  La remuneración
salarial por Facebook no solo tiene una perspectiva revolucionaria, sino
que además tiene una perspectiva revolucionaria desde un posicionamiento
contemporáneo que apunta hacia la solidaridad de clase.

Es importante reconocer que cuando se trata de Facebook no se trata de
un trabajo como cualquier otro, sino que se trata de la manipulación más
profunda, la violencia más sutil y mística que el capitalismo ha
perpetrado contra nosotres.  Es verdad que bajo el capitalismo tode
trabajadore es manipulade y explotade y su relación con el capital ya
está totalmente mistificada.

El salario da la impresión de un trato justo: nos pagan porque
trabajamos, por lo tanto estamos en igualdad con nuestros jefes, pero en
realidad el salario antes que ser la paga por el trabajo que realizamos,
oculta todo el trabajo no remunerado que es apropiado como ganancia.  El
salario al menos reconoce que somos trabajadorxs y podemos negociar y
luchar alrededor y contra los términos y cantidades de ese salario y los
términos y cantidades de nuestro trabajo.

Obtener un salario significa ser parte de un contrato social y no hay
ninguna duda en relación a su significado: trabajamos, no porque nos
guste o porque nos surge naturalmente, sino porque es la única condición
bajo la que nos es permitido vivir.  Pero la explotación del trabajo no
es nuestra identidad.

Demandar salarios por nuestra actividad en Facebook socava las
expectativas que la sociedad tiene de nosotres, porque esas expectativas
--la esencia de nuestra socialización-- son funcionales a nuestro estado
conectado no asalariado.  En este sentido es mejor comparar la lucha de
las mujeres por los salarios que la lucha de los obreros cisvarones por
mejores salarios.  Cuando luchamos por salarios luchamos sin
ambigüedades y directamente contra nuestra explotación social.  Luchamos
por romper el plan del capital a través del cual ha sido capaz de
perpetuar su poder: monetizar nuestros afectos, sentimientos y tiempo
libre.

"Salarios por Facebook" entonces es una demanda revolucionaria no porque
destruya por sí misma el capital, sino porque lo ataca y lo fuerza a
re-estructurar las relaciones sociales en términos más favorables para
nosotres y en consecuencia más favorables para la solidaridad de las
clases trabajadoras.  De hecho, demandar salarios por Facebook no
significa que si nos pagaran seguiríamos haciéndolo.  Significa
exactamente lo contrario.

Decir que queremos dinero por Facebook es el primer paso hacia reusarnos
a trabajar para ellos, porque la demanda de un salario visibiliza
nuestro trabajo, condición indispensable para comenzar a luchar contra
este.  Contra cualquier acusación de "economicismo" recordemos que el
dinero es capital, es decir que es el poder de comandar trabajo.

Por lo tanto re-apropiarnos de ese dinero fruto de nuestro trabajo --y
el de nuestros afectos-- significa también socavar el poder del capital
de comandar trabajo forzado de nosotres.

Posicionándonos desde el trabajo podemos demandar no un solo salario
sino muchos salarios, porque hemos sido forzades a realizar muchos
trabajos.  También trabajamos para Google, Instagram, Twitter,
Microsoft, Youtube y miles de otros.  Desde ahora mismo demandamos
dinero por todos esos trabajos, para que podamos rechazar alguno de
ellos y eventualmente todos.

"Salarios por Facebook" es solo el principio, pero su mensaje es claro:
desde este momento tienen que pagarnos porque como usuaries no les vamos
a garantizar nada más.  Queremos llamar trabajo a lo que es trabajo para
eventualmente poder re-descubrir lo que son los afectos.
