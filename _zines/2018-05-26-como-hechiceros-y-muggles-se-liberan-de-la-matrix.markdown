---
title: "Cómo hechiceros y muggles se liberan de la Matrix"
author: "Doc Searls"
layout: post
signature: 0
---

> La siguiente nota fue escrita por Doc Searls, quién trabaja activamente en
> [Customer Commons](http://customercommons.org/terms). Publicada en Linux
> Journal en
> [abril](https://www.linuxjournal.com/content/how-wizards-and-muggles-break-free-matrix)
> y [enero](http://www.linuxjournal.com/content/every-user-neo) de 2018 con
> mínima diferencia respecto del final. En esta traducción hemos reunido lo mejor de
> ambas.

# Cómo hechiceros y 'muggles' se liberan de la Matrix

Primero inventamos un mundo en el que todas pudieran ser libres. Después
ayudamos a levantar castillos feudales en ese mundo, para que todas pudieran
vivir en ellos. Ahora es el momento de hacer explotar esos castillos, dándole a
todas muchas mejores formas de usar la libertad que las que podrían disfrutar en
un castillo.

Voy a mezclar un par de metáforas peliculeras. Van a darse cuenta de por qué lo
hago.

En abril de 1999, un par de semanas después de que estrenaran "Matrix", todo el
staff de Linux Journal fue a verla en un cine cercano a nuestras oficinas de
aquel momento, en el barrio Ballard de Seattle. Aunque nos dimos cuenta casi de
inmediato de que la película era una nerdeada clásica (onda, el héroe era un
programador estrella); también parecía estar claro que la cuestión referida en
el título de la película –un mundo falso pero hiper-convincente, habitado por
casi toda la especie humana– era una alegoría (término que la
[Wikipedia](https://es.wikipedia.org/wiki/Alegor%C3%ADa) define como "metáfora
cuyo vehículo puede ser un personaje, lugar o evento que representa problemas y
sucedáneos del mundo real").

<!-- TODO foto de la píldora roja -->

Una primera y obvia interpretación era en términos religiosos. Neo era una
figura mesiánica como Cristo, resucitado por un personaje de nombre Trinity,
quien hizo el papel de Espíritu Santo después de que a Neo lo asesinara el casi
satánico Agente Smith... todo esto mientras los pocos humanos que no habían sido
esclavizados aún por las máquinas vivían en una ciudad subterránea llamada Zion.

Cuando en los años que siguieron aparecieron las dos secuelas, varios tramos de
la historia también parecían encontrar inspiración en otras religiones: Budismo,
Hinduismo e incluso Gnosticismo. Como los hermanos Wachowski, que escribieron y
dirigieron las películas, se han convertido desde entonces en las hermanas
Wachowski, uno también puede hallar, retrospectivamente, varias claves trans de
la trilogía.

<!-- TODO habilita comentario u nota al pie, barca transfem? -->

Además están los temas filosóficos que se tocan. Los cautivos en la Matrix creen
que el mundo en el que viven es real, de la misma forma en que aquellos viviendo
en la [Alegoría de la
Caverna](https://es.wikipedia.org/wiki/Alegor%C3%ADa_de_la_caverna) de Platón
creían que las sombras proyectadas en el muro de la cueva eran reales porque no
podían ver que la fuente de luz que las proyectaba era un fuego encendido detrás
de sus cabezas. En la historia de Platón, uno de los prisioneros se escapa y
visita el mundo real. En Matrix, ese prisionero es Neo, su nombre un anagrama
para The One (el elegido), quien tiene el deber de rescatar a todas o al menos
salvar a la ciudad de Zion (spoiler: lo hace).

Pero yo no me creí nada de eso, porque ya había visto al marketing trabajando
para convertir el mundo online, libre y abierto, en un hábitat comercial donde
–tal y como pasaba en ficcional Matrix– los seres humanos eran reducidos a
ocupar el lugar de simples baterías para alimentar a gigantescas máquinas robóticas
que cosechaban la atención humana, para procesarla y alimentar de nuevo a los
hombres con ella, una y otra vez.

Este era el diseño básico del mundo que el marketing quería imponernos en la era
digital: uno en el que cada uno de nosotras fuera un “blanco”, en el que
pudiésemos ser “capturados”, “adquiridos”, “controlados”, “administrados” y
“asegurados” para que el “contenido” y las “experiencias” personalizadas
pudiesen llegar hasta nuestros oídos y globos oculares. Los trabajadores de
marketing ya hablaban en estos términos mucho antes de que apareciera internet,
pero con cada globo ocular repentinamente disponible para recibir
_personalmente_ la información, la ansiedad por conectarnos a todas en la Matrix
del marketing se volvió irresistible.

De hecho, una de las razones por la que cuatro de nosotras posteamos el
[Manifesto Cluetrain](http://cluetrain.com/) en la world wide web durante ese mismo mes de abril fue porque
queríamos dejar en claro que la internet era para todos, no solo para hacer
marketing.

Pero aunque Cluetrain haya sido muy popular (en especial entre los gerentes de
mercadotecnia), el marketing logró que los ingenieros –incluidos varios
hechiceros de Linux– construyeran una Matrix para encerrarnos a nosotras.
Estamos viviendo en ella ahora mismo. Al menos que tengas tu hardware y tu
software configurados en modo privacidad absoluta mientras navegás por el mundo
online (¿y encima podés estar seguro de estar realmente seguro?), estás en la
Matrix del marketing.

Las instancias más obvias de esa Matrix son mantenidas por Google, Facebook,
LinkedIn, Twitter, Tumblr, Pinterest, Amazon y muchas otras. Pero mucho más está
provisto por nombres de los que nunca escuchaste hablar. Para saber en qué
andan, equipá a tu navegador con algún addon capaz de darle nombre a las fuentes
donde se originan los archivos de rastreo. (Ejemplos son: [Privacy
Badger](https://www.eff.org/privacybadger) o
[Lightbeam](https://addons.mozilla.org/en-US/firefox/addon/lightbeam/).) Luego
dirigite con el navegador al sitio de alguna fuente de noticias cuya oficina
comercial haya sido asimilada por ese Agente Smith llamado "adtech": Por
ejemplo, el sitio de [The Los Angeles Times](http://www.latimes.com/). Una vez ahí, revisá tu herramienta
de protección contra el rastreo para ver una lista de todas las entidades
tratando de espiarte.

Acá hay solo algunos de los 57 sospechosos que Privacy Badger encontró en la
página principal del LA Times:

* yieldmo.com
* trbas.com
* trbimg.com
* trbdss.com
* steelhousemedia.com
* teads.tv
* trb.com
* truste.com
* revjet.com
* rflhub.com
* rubiconproject.com
* steelhousemedia.com
* moatads.com
* ntv.io
* openx.net
* postrelease.com
* keewee.co
* krxd.net
* mathtag.net
* ml314.net
* indexwww.com
* ixiaa.com
* ensighten.com
* everesttech.net
* tronc.com
* sitescout.com
* jquery.com
* bootstrapcdn.com
* bouncexchange.com
* chartbeat.com
* cloudfront.net
* agkn.com
* adsrvr.org
* gomnit.com
* responsiveads.com
* postrelease.com

Muchos de estos aparecen más de una vez, con diferentes prefijos. También dejé
afuera del recorte a las distintas variantes de google, doubleclick, facebook,
twitter y otros nombres familiares.

Interesante: si la reviso una segunda, tercera o cuarta vez, la lista cambia,
supongo que debido a que los servidores de publicidad tercerizados están
ocupados tratando de meter de nuevo más rastreadores en mi navegador mientras la
página siga abierta.

[Busqué](https://www.google.com/search?&q=moatads) a uno de esos rastreadores,
"moatads", escogido al azar, y la mayoría de los 1.820.000 de resultados que
encontré eran sobre cómo moatads era algo malo. En orden, esta es la primera
página de resultados:

* Quitar el virus Moatads (Guía para eliminarlo) - Oct 2017 - 2 Spyware
* Eliminar Moatads Malware (¿Qué es moatads?) Marzo 2018 ...
* Qué es z.moatads.com? - Webroot Community
* moatads.com
* Cómo eliminar completamente Moatads.com - It-Help.info
* Desinstalar virus Moatads (Guía de desinstalación) - Oct 2017
* Moatads Malware Removal | Mobile Security Zone
* Moatads Removal Guide | Spyware Techie
* Moatads sigue apareciendo en mi computadora y ya es un problema serio... cómo me deshago de él?

El cuarto ítem en la lista señala que la compañía detrás de moatads, moat.com,
"mide índices de atención en tiempo real más de 33 mil millones de veces por día".
Esa es nada más que una de las empresas que colaboran en la construcción de la
Matrix.

Claramente no hay un Arquitecto ni una Oráculo sosteniendo a esta Matrix, sino
el diseño no sería tan malo. Y aunque eso sea una ventaja para nosotros, todavía
estamos atrapados en un mundo online donde espiar es la norma, nunca la
excepción, y donde la autonomía personal suele estar confinada a los castillos
de grandes proveedores de servicios, las “redes sociales” y los productores de
equipamiento altamente propietario. [Entendiendo como propietario a todo
software que no es libre ni abierto.]

Allá por el año 2013, [Shoshana Zuboff](http://shoshanazuboff.com/) dijo que
[éramos "la fricción" contra "los nuevos Señores del
Anillo"](http://www.faz.net/aktuell/feuilleton/the-surveillance-paradigm-be-the-friction-our-response-to-the-new-lords-of-the-ring-12241996.html).
En ensayos posteriores definió de dos maneras al sistema de publicidad basado en
espiar: [capitalismo de vigilancia y El Gran
Otro](http://www.faz.net/aktuell/feuilleton/debatten/the-digital-debate/shoshana-zuboff-secrets-of-surveillance-capitalism-14103616.html?printPagedArticle=true#pageIndex_0) [nota del traductor: se perdió la rima big brother & big other].
Si las cosas se dan según lo planeado, su próximo libro, The Age of Surveillance
Capitalism: The Fight for a Human Future at the New Frontier of Power, saldrá
dentro de poco.

La gente ya se está organizando para resistir, aunque todavía no lo sepan. El
[reporte de 2017 de PageFair sobre
Adblock](https://pagefair.com/blog/2017/adblockreport) asegura que al menos el
11% de la población mundial está bloqueando las publicidades en al menos 615
millones de dispositivos. [GlobalWebIndex dice
que](https://blog.globalwebindex.net/chart-of-the-day/37-of-mobile-users-are-blocking-ads),
en enero de 2016, el 37% de todos los usuarios de teléfonos móviles bloqueaban
anuncios y un 42% que no lo hacía aún, pensaba en hacerlo. [Statista dice
que](https://www.statista.com/statistics/274774/forecast-of-mobile-phone-users-worldwide)
el número de usuarios de teléfonos celulares en el mundo alcanzó los 4770
millones en algún momento de 2017. Si combinamos estas dos cifras, llegamos a la
conclusión de que más de 1700 millones de usuarios ya están bloqueando
publicidades, un número que excede el total de la población del hemisferio
occidental. Por todo esto fue que definí el adblocking como ["el boycott más
grande del
mundo"](http://blogs.harvard.edu/doc/2015/09/28/beyond-ad-blocking-the-biggest-boycott-in-human-history)
allá por 2015. Hoy en día tiendo a pensarlo más como una revuelta de esclavos.

Pero necesitamos ser más que esclavos liberados. Necesitamos, como sugiere
Shoshana, ser los amos de nuestras propias vidas y de todas las relaciones que
mantenemos online.

En la película Matrix, Morfeo le pregunta a un Neo todavía cautivo si creía en
el destino. "No", dice Neo, "porque no me gusta la idea de que no tengo el
control de mi vida".

Nunca podremos tener control sobre nuestras vidas mientras esas vidas sean
vividas dentro de castillos corporativos y nos falten las herramientas para
dominar nuestros virtuales cuerpos y mentes online.

No importa si Facebook, Google y el resto no tienen malas intenciones, o si en
realidad no pretenden "Unir al mundo", u "Organizar la información del mundo y
volverla universalmente útil y accesible", o "desarrollar servicios que mejoren
significativamente las vidas de la mayor cantidad posible de gente". Necesitamos
ser agentes libres e independientes de nosotros mismos.

Eso no puede suceder dentro de los sistemas cliente-servidor que están online
desde 1995 y un poco antes también, sistemas que bien podrían ser llamados de
esclavo-amo. No puede suceder mientras tengamos que clickear sobre el
"Acepto" en los términos y condiciones de sistemas esclavo-amo que rigen por
defecto al mundo online. No puede suceder mientras todo lo útil en el mundo
online requiera un login y un password. Todas esas imposiciones son paredes en
lo que Morfeo llama una "prisión para tu mente".

Tenemos que pensar y trabajar fuera de las paredes de esas prisiones (los ex
castillos). Y no alcanza con liberarnos a nosotras mismas. Para usar otra
metáfora de una película, _es hora de que los magos liberen a los muggles_.

Al final de la trilogía de Matrix, Neo impide que el programa viral del Agente
Smith destruya ambos mundos, el de las máquinas y el humano. Pero no se nos
explica qué va a hacer la gente que estaba conectada a la Matrix una vez que
fueran libres; ni siquiera se nos dice si la libertad de esa gente estaba en los
planes. De hecho, todo lo que Neo hace es salvar a Zion y dejar al resto de la
humanidad viviendo en la vieja Matrix: una existencia ilusoria mantenida por
máquinas donde el único propósito real era servir como fuente de energía para
ese mismo aparato.

Afortunadamente, internet no es Hogwarts. Aunque sea un producto de la magia,
todos, incluso los magos, viven y piensan dentro de sus muros. Pero la
tecnología digital y la internet fueron diseñadas para la libertad, y no para
más esclavitud bajo el modelo industrial.

Personalmente considero que toda usuaria es una Neo en potencia. Desarrollar
esta potencia implica una revolución enorme, la mas grande desde la revolución
industrial (que fue cuando comenzamos a subordinar el [contra]poder personal a
los requerimentos de las grandes empresas).

Seguir masticando la píldora azul es decisión de cada una. Hagamos que la
civilización online sea algo mejor que el sistema digital feudal que tenemos
ahora.
