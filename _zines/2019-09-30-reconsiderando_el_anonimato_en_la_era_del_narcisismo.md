---
title: "Reconsiderando el anonimato en la era del narcisismo"
author: "Gabriella Coleman"
layout: post
signature: 0
---

# Reconsiderando el anonimato en la era del narcisismo

**Por Gabriella Coleman**

> No todo son identidades robadas, MDMA ni pornografía infantil.

> Para todos los nombres en la historia: Ha llegado el tiempo de
> sacrificar ese nombre. --Anonymous (Manifiesto del nómada anónimo)

> El ego y la fama son, de forma predeterminada, contradictorios
> inherentemente al anonimato. El pasto más alto es el que se corta
> primero. Sea #Anonymous.  --Anonymous (\@YourAnonNews), 16 de abril de
> 2012.

La premisa de esta colección es que la privacidad y el anonimato se
están desvaneciendo bajo el violento ataque de la vigilancia
gubernamental y corporativa. La premisa no es nueva, en 2009 muches
defensores, activistes, bibliotecaries y libertaries civiles estaban
encontrando imposible de imaginar que la privacidad y el anonimato
fueran a existir en el futuro cercano. Este era el tiempo en que los
ejecutivos de Silicon Valley estaban construyendo la infraestructura
digital de vigilancia capitalista y defendiéndola mediante la estrategia
de hacer ver a la privacidad como moralmente dudosa. Por ejemplo, cuando
una periodista le preguntó a Eric Schmidt de _Google_ si deberíamos
confiarles nuestros datos, su respuesta condescendiente fue calculada
para eliminar cualquier valencia positiva hacia la privacidad: "Si tiene
algo que no quiere que nadie sepa, entonces tal vez no debería estar
haciéndolo en primer lugar".

Pero en ese momento se hizo prominente una misteriosa colectiva portando
el nombre _Anonymous_ - un movimiento global de protesta y de caracter
remoto que promovía la idea de que las identidades anónimas podrían
organizarse para luchar mediante comportamientos éticos y una oposición
rotunda hacia aquellos que solamente buscaban notoriedad. Aunque al
inicio fue usado por trolls anónimos para coordinar travesuras puntuales
de acoso a lo largo de la internet, el apodo _Anonymous_ tomó nuevos
significados en 2008 cuando un grupo de participantes se vieron
involucrades en una serie escalonada de hackeos y operaciones políticas
diseñadas para la adopción mediática. Figuras identificándose como
_Anonymous_ usaron su conocimiento técnico y su sentido algo troll del
espectáculo mediático para llamar a moratoria a la caza de ballenas
japonesa y noruega; para demandar justicia para víctimas de ataques
sexuales y brutalidad policial, incluso revelando algunas veces los
nombres de los presuntos perpetradores; hackear gobiernos y
corporaciones por igual; asistir las ocupaciones de Egipto, Túnez,
España y Estados Unidos; soportar el levantamiento sirio; "doxear" a
oficiales de policía que rociaron con pimienta a les protestantes;
exponer pedófilos _on line_; e incluso brindar ropas a las personas sin
hogar.  Las agencias de noticias llegaron a contar con _Anonymous_ para
un flujo constante de historias sensacionalistas. Una banda afiliada
llamada _LulzSec_ se dedicó a entregar un nuevo «hackeo diario» por
cincuenta días. Al infiltrarse en _Sony Pictures_, publicar noticias
falsas en el sitio web de _PBS_ y robar correos electrónicos de la
organización _Arizona Public Safety_, sirvieron pasto a la prensa
mientras alegremente auto reportaban sus hazañas en redes sociales a una
base de fans creciente y satisfecha. "En las últimas pocas semanas han
ganado cerca de 96000 seguidores de Twitter. Eso es 20000 más que cuando
revisé ayer. Twitter le ha dado a _LulzSec_ un escenario para presumir,
y están presumiendo", escribió una investigadora de seguridad.
_Anonymous_ consiguió cortejar incluso más controversia con los trucos
rituales como "Viernes de joder al FBI", que vio a les hacktivistes ir a
_Twitter_ al final de cada semana y burlarse de la agencia encargada de
exponer a sus miembros. Para una antropóloga que estudia las culturas
del hackeo y la tecnología, este fue un momento vigorizante, yo estaba
pegada a mi silla.

Pero conforme el momento ejemplar pasó, la historia de _Anonymous_ viró
hacia lo irónico y por último, incluso a lo trágico, cuando les
participantes centrales fueron traicionades y arrestades, y el nombre
comenzó a prestarse para operaciones militares - como campañas anti
terrorismo en servicio del estado nación - a las que muches de sus
miembrxs anteriores se habían opuesto vehementemente en varias
ocasiones. Dado el poder omnívoro de la máquina contemporánea de
vigilancia digital para extraer datos de humanes y luego usarlos en
nuestra cotra, yo nunca fui tan ingenua como para creer realmente que
_Anonymous_ pudieran ser nuestres salvadorxs. Mi opinión fue más
humilde: estaba más que nada maravillada de la forma en que estes
disidentes enmascarades abrazaron el anonimato como un tipo de ética
para prevenir comportamientos de pavoneo social y motivar a les
participantes en un silencio solidario antes que la búsqueda de crédito
individual, incluso cuando estaban acosadas, y buscaban la publicidad
colectiva, por sus hackeos, bromas y protestas épicas.  Ciertamente
ayudó que _Anonymous_ contribuyera a un número de causas políticas que
yo apoyaba, como _Ocupy Wall Street_, la exposición de firmas de
vigilancia, y la lucha contra la corrupción gubernamental. Yo aprecié
que grupos de personas estuvieran tomando el manto del anonimato en gran
parte para bien - incluso si parecía que podría ser por una última vez
antes de que el anonimato mismo se disipara del todo.

Mi pesimismo acerca de la viabilidad del anonimato y la privacidad, que
sobrevivan (y que prosperen), aún domina mi optimismo generalmente. Pero
incluso conforme han ido menguando los días gloriosos de _Anonymous_,
finalmente se ha incorporado un movimiento de anonimato y privacidad
ligeramente más fuerte. En parte gracias a la filtración masiva de
documentos de la NSA de Edward Snowden, que brindó una prueba mucho más
fuerte de la que existía anteriormente sobre la vigilancia gubernamental
y su confabulación con el sector privado, ahora se está librando una
fuerte batalla para preservar la privacidad y el anonimato. Un poco
después de las revelaciones de Snowden, incontables proyectos
tecnológicos dirigidos por hackers galvanizaron su desenmascaramiento,
desarrollaron herramientas para aumentar la privacidad en la que
periodistas, víctimas de violencia doméstica, trabajadorxs de derechos
humanos y disidentes polítiques ahora confían para moverse por el mundo
con más cuidados. La usabilidad de estas herramientas ha mejorado
considerablemente. Mientas hace cinco años yo sufría para recomendar
herramientas simples de autodefensa digital a amigues y familiares, hoy
puedo señalar a _Signal_ (una aplicación para mensajes de texto y
llamadas cifradas), el navegador _Tor_ (que anonimiza el tráfico web),
un media docena más de otras aplicaciones; cada una ha recolectado un
financiamiento y un compromiso crecientes gracias al aumento en el
escrutinio de las violaciones a la privacidad por parte de los estados y
corporaciones. Aunque _Google_ anunció que implementaría cifrado
estricto de punta a punta en sus servidores para asegurar que los datos
de los que depende para avivar su empresa comercial no sean tan
fácilmente accesibles para otras, aún están en proceso de realizar estos
cambios. Las organizaciones que conjugan política, tecnología y
activismo, como _Electronic Frontier Foundation_, _Fight for the
Future_, el _Library Freedom Project_, _Big Brother Watch_ y _Privacy
International_ también han ayudado a asegurar que la privacidad se
mantenga en marquesina como un asunto político.  Un flujo constante de
escándalos nuevos, como las revelaciones de que _Cambridge Analytica_
usó datos personales recolectados de _Facebook_ para influenciar
resultados de elecciones, ha amplificado estas preocupaciones y
demostrado la extensión con la cual cuestionamientos sobre el uso de
datos personales y privacidad se mantienen vigentes.

Como miembre de una confederación libre de defensorxs del anonimato,
rutinariamente doy conferencias sobre las formas en que el anonimato
puede habilitar procesos democráticos desde las disidencias y la
denuncia de irregularidades. En el curso de ese proselitismo se ha
vuelto aparente que el anonimato por lo general es más difícil de
defender que otras libertades civiles estrechamente relacionadas como
libertad de expresión y la privacidad.  El anonimato tiene mala
reputación. Y no es difícil ver por qué: los usos más visibles del
anonimato _on line_, como comentarios en foros, tienden hacia lo tóxico.
Numerosos periódicos en años recientes han eliminado esos foros, los han
controlado o reconfigurado, atentes a las formas en que por lo general
fallan en engendrar una discusión civil y en lugar de esto reproducen
más discurso dañino y de odio. El anonimato de forma similar permite que
trolles en redes sociales esquiven la responsabilidad mientras atacan de
forma brutal (en su mayoría) a personas de color, mujeres y disidencias.

Las connotaciones negativas con que muches piensan el anonimato son
evidentes en su percepción de lo que periodistas y alarmistas llaman la
_dark web_.  Cuando pregunto a mis estudiantes qué piensan que pasa ahí,
muches la describen como la esquina más siniestra de la red, infestada
de tipos pervertidos y peligrosos que heckean bilis en nuestros
dispositivos, supurando y eructando en mini volcanes de pasaportes
robados, cocaína y pornografía infantil. Algunes incluso creen que ser
anónimes en Internet es equivalente - en cada instancia - a trasegar en
la _dark web_. La metáfora de la oscuridad ha funcionado claramente para
implantar imágenes erróneas y nefastas en sus mentes, entonces yo
contrarresto con una imagen diferente.

Como mis estudiantes tienen poco conocimiento de cómo funciona el
anonimato, primero les explico que lejos de ser una decisión binaria
como un interruptor de luz que se enciende y apaga, el anonimato
típicamente involucra un surtido de opciones y gradientes. Muchas
personas se esconden a sí mismas sólo por el nombre _on line_, nombre de
usuarie, alias, sobre nombre, avatar o del todo sin atribución:
«anónima». Este anonimato social tiene que ver solo con la atribución
pública y protege el nombre legal de le participante, mientras que otra
información identificante, como la dirección IP, puede aún ser visible a
una red de observadores como les administradores de sistemas que
ejecutan el sitio en el que el contenido es publicado. No hay una única
herramienta de anonimato divina que brinde protección omnipotente,
infalible, fiable y a prueba de tontos con la capacidad de esconder cada
rastro digital, revolver todo el tráfico de red y envolver todo el
contenido en un caparazón de cifrado. Lejos de esto: el anonimato
técnico perfecto se considera como un arte demandante y exigente que
causa pérdida de sueño incluso a los hackers más selectos. Une usuarie
que busca anonimato técnico debe entretejer un surtido de herramientas,
cuyo resultado será una colcha de protección más o menos robusta y
determinada por las herramientas y la habilidad de le usuarie.
Dependiendo de cuántas herramientas se usen, esta colcha de protección
podría esconder toda la información identificante o solamente algunos
elementos esenciales: el contenido de los mensajes intercambiados, la
dirección IP originaria, las búsquedas en el navegador web o la
ubicación de un servidor.

El mismo anonimato, continuo y usado por el criminal o el matón o el
acosador, es también usado como una «arma del pobre» en la que confían
personas ordinarias, informantes, víctimas de abuso y activistas para
expresar opiniones políticas controversiales, compartir información
sensible, organizarse, brindar protección contra la represión estatal y
construir santuarios de soporte.  Afortunadamente, no hay escasez de
ejemplos que iluminen los beneficios derivados de la protección del
anonimato: pacientes, parientes y sobrevivientes se reunen en foros de
internet como _DC Urban Moms and Dads_ para discutir temas sensibles
usando alias, lo que permite discusiones francas de lo que de otra forma
podrían ser temas estigmatizantes. Víctimas de abuso doméstico, espiadas
por sus perpetradores, pueden técnicamente cubrir sus rastros digitales
con el navegador _Tor_ y buscar información acerca de refugios. Las
informantes hoy son empoderadas para protegerse a sí mismas como nunca
antes, dada la disponibilidad de buzones digitales como _SecureDrop_,
ubicados en lo que es llamado un servidor «onion», u oculto. Estos
puntos de entrega, que facilitan la compartida de información de forma
anónima; ahora son hospedados por docenas de sedes periodísticas
establecidas, desde _The Guardian_ a _The Washington Post_.  Hospedar
datos en servidores «onion» accesibles sólo a través de _Tor_ es un
mecanismo efectivo para contrarrestar la represión y la censura
perpetradas por el estado. Por ejemplo, activistas iraníes crítiques al
gobierno protegieron sus bases de datos haciéndolas disponibles sólo
como servicios «onion». Esta arquitectura hace que el gobierno pueda
incautar el servidor web conocido de forma pública, pero que no pueda
encontrar el servidor que brinda el contenido de la base de datos.
Cuando los servidores web son desechables, el contenido es protegido y
el sitio con la información dirigida a empoderar activistas puede
reaparecer _on line_ de forma rápida, forzando a que las intenciones de
censura del gobierno tengan que jugar al «whac-a-mole». Al confiar en
una suite de tecnologías de anonimato, les hacktivistas pueden descubrir
información política de importancia al transformarse a sí mismes en
fantasmas imposibles de rastrear: por ejemplo, un grupo se infiltró de
forma anónima en salas de chat de supremacistas blancos luego del
trágico asesinato de Heather Heyer y robaron las bitácoras que
detallaban los trabajos de grupos de odio organizados para el encuentro
de _Charlottesville_, así como sus viles reacciones y luchas internas.

Aún así, es cierto que cosas terribles se pueden lograr bajo la cubierta
del anonimato técnico. Pero es necesario recordar que el estado está
dotado con un mandato y tiene recursos significativos para cazar
criminales, incluyendo a aquelles envalentonades por la invisibilidad.
Por ejemplo, en 2018 el FBI solicitó cerca de 21.6 millones de sus $8
mil millones de presupuesto anual para su programa _Going Dark_, usado
para "desarrollar y adquirir herramientas para el análisis de
dispositivos electrónicos, capacidad de criptoanálisis y herramientas
forenses". El FBI puede desarrollar o pagar costosas programas que
aprovechen vulnerabilidades de seguridad o herramientas de hakceo, que
han usado para infiltrarse y tomar control sobre sitios de pornografía
infantil, como hicieron en 2015 con un sitio llamado _Playpen_. Sin duda
alguna, el estado debería tener la capacidad de luchar contra
criminales. Pero si se les brindan capacidades de vigilancia sin
restricciones como parte de esa misión, les ciudadanes perderán la
capacidad de ser anónimes y el gobierno se arrastrará hacia el fascismo,
que es otro tipo de criminalidad. Les activistas por otra parte, que son
en gran medida pobres de recursos, con frecuencia son puestes en la mira
de actores estatales de forma injusta y por lo tanto requieren
anonimato. De hecho, el anonimato permite hablar y organizarse sin
interferencias a activistas, fuentes y periodistas -como es su derecho-,
que aún no están en la mira del estado.

La importancia, usos y significado del anonimato dentro de una entidad
activista como _Anonymous_ es menos clara que en mis anteriores
ejemplos. Esto puede surgir en parte del hecho que _Anonymous_ es
confuso. El nombre es un alias compartido que está libre para que
cualquiera lo tome, lo que Marco Deseriis define como un "nombre
impropio". Disponible de forma radical para todas las personas, este
tipo de etiqueta se dota con una susceptibilidad ya incorporada hacia la
adopción, circulación y mutación. El público con frecuencia no está al
tanto de quiénes fueron _Anonymous_, cómo trabajaron y cómo
reconciliaron sus distintas operaciones y tácticas. Hubo cientos de
operaciones que no tuvieron relación entre ellas y que con frecuencia no
estaban alineadas ideológicamente --algunas en apoyo firme a la
democracia liberal, otras buscando destruir el estado liberal en favor
de formas de gobierno anarquistas.  También es por esta razón que
"Anonymous no es unánime" se volvió una broma popular entre les
participantes, recordándole a les espectadores el carácter
descentralizado y sin liderazgo del grupo, y señalando la existencia de
desacuerdos sobre tácticas y creencias políticas.

Para miembros del público, así como mis estudiantes, su valoración de
las operaciones de _Anonymous_ con frecuencia dependían de su reacción a
una de las cientos de operaciones con las que se hayan podido cruzar, su
percepción de la figura de Guy Fawkes y otras idiosincrasias como su
opinión sobre la justicia de vigilantes o acción directa. Mientras
algunes espectadores adoraron su buena disposición de rebelarse contra
la autoridad, otres estuvieron horrorizades por su prontitud a romper la
ley con tal impunidad.  Entre una cacofonía de posiciones sobre
_Anonymous_, yo siempre encontré una categoría de resistencia a
respaldar a _Anonymous_: el tipo bueno y legítimo (profesorxs de derecho
académico o estudioses de la política liberal, por ejemplo), siempre
escéptiques y consternades de _Anonymous_ en su totalidad por una
pequeña cantidad de operaciones de justicia vigilante que se realizaron
bajo su manto. La cosa extraña era la forma en que esos tipos legítimos
encontraron un acuerdo con una más pequeña, pero vocal, clase de
activistas de izquierda - aquelles entusiastas de apoyar maniobras de
acción directa pero llenas de reservas cuando eran llevadas a cabo de
forma anónima. Ellas tendían a estar de acuerdo en una creencia en
particular: que la gente que abraza el anonimato con el propósito de
actuar (y no simplemente de hablar), -en especial cuando tales acciones
rodean el proceso debido- son personajes turbios por defecto porque el
anonimato tiende a anular la rendición de cuentas y por lo tanto la
responsabilidad; que la máscara en sí misma es un tipo de mentira
encarnada, refugiando cobardes en quienes simplemente no se puede
confiar y quienes no son responsables de las comunidades a las que
sirven.

Pero estos argumentos ignoran la variedad y usos justos del anonimato
que _Anonymous_ puso en servicio de la honestidad y la nivelación
social. Con la distancia concedida por el tiempo, mi convicción de que
_Anonymous_ ha sido generalmente una fuerza digna de confianza y une
embajadore encomiable por el anonimato es hoy aún más fuerte. Incluso si
su presencia ha decaído, han dejado atrás una serie de lecciones acerca
de la importancia del anonimato que en la era de Trump, son más vitales
que nunca para tener en cuenta. De estas lecciones, consideraré aquí los
límites de transparencia para combatir a la desinformación y la
capacidad del anonimato para proteger a las personas honestas, así como
su habilidad para minimizar los daños de la celebridad desenfrenada.

## Lección 1: La transparencia no es una panacea para la desinformación

Primero consideremos el poder de _Anonymous_ y el anonimato a la luz del
clima político contemporáneo, con periodistas, comentaristas y
activistas en una crisis existencial turbulenta acerca de la confianza,
la verdad, y noticias basura. Déjenme declarar desde el principio que
demandar transparencia, en mi libro de jugadas políticas, está alto en
la lista de tácticas oportunas que pueden ayudar a animar las
actividades políticas. Buscar transparencia de personas, corporaciones e
instituciones que podrían tener algo malo que ocultar, y la influencia
para lograr ocultarlo, ha servido en incontables circunstancias para
avergonzar a estafadorxs y oportunistes de forma que salgan de sus
codiciadas posiciones de poder (y yo con resolución defiendo el
anonimato por su capacidad de engendrar transparencia). Aún así, la
efectividad de la demanda de transparencia y honestidad con frecuencia
ha sido exagerada, y sus defensorxs algunas veces de forma ingenua
atribuyen una fe casi mágica a tal táctica mientras consideran inmorales
los medios anónimos para el mismo fin de la honestidad. En el pasado,
cuando he discutido la importancia del anonimato y los límites de la
demanda de transparencia en busca de la verdad, muy pocas personas me
tomaron del todo en serio, al margen de un pequeño grupo de académicxs y
activistas ya imbuides en hacer afirmaciones similares. Todo esto cambió
cuando Donald Trump se volvió el presidente. De repente era mucho más
fácil ilustrar la lógica detrás de la famosa ocurrencia de Mark Twain:
"La verdad es poderosa y prevalecerá. No hay nada malo con esto, excepto
que no es así".

El sentido común periodístico, aún intacto en gran medida llegando a la
elección, dictó que refutar falsedades preservaría la integridad del
mercado de ideas - la arena en la que la verdad, dado suficiente tiempo
al aire, puede borrar mentiras. Sin embargo, después de que Trump ganó
la elección, muchas periodistas fueron forzadas a confrontar el hecho de
que el sentido común, como tan astutamente lo puso el antropólogo
Clifford Geertz, es "lo que la mente llena de supuestos... concluye".
Para las críticas, los fallos morales de Trump son obvios en su
comportamiento vil y mentiras patológicas, ambas grabadas de forma
meticulosa por periodistas. _The Washington Post_ ha rastreado las
declaraciones falsas o engañosas desde su primer día en función, y
encontraron que su celo por mentir sólo ha crecido con el tiempo. Sin
embargo, aunque sus partidaries también distinguen a Trump como audaz,
están armades con un conjunto distinto de supuestos y por lo tanto
llegan a conclusiones radicalmente distintas sobre su carácter y
acciones. En la misma auditoría de declaraciones falsas de Trump en _The
Washington Post_, une comentarista _on line_ muestra cómo algunes de sus
defensores están dispuestes a pasar por alto sus mentiras,
interpretándolo como auténtico y emocionalmente cercano comparado con el
político típico: "Trump con frecuencia es hiperbólico y leva sus
sentimientos en su manga para que todos lo vean, refrescante dirán
algunos. Uno con frecuencia se pregunta si para él es tan siquiera
posible ser tan hipócrita como el político típico. Su corazón y
políticas parecen estar en el lugar correcto".

Apelando a aquelles que desconfían del entorno político contemporáneo,
algunes de les seguidorxs más acérrimes de Trump argumentan que él sirve
a un propósito más alto y noble al sacudir el poder establecido. Incluso
aunque el sentido común "varía de forma dramática de una persona a la
siguiente", como dijo Geertz, Trump aun ha logrado secuestrar nuestra
atención colectiva, provocando a los medios para que cubran cada uno de
sus movimientos, con frecuencia a través de una actuación de
autenticidad falsa pero convincente. Ya sea en horror, diversión o
adulación, el público estadounidense se mantiene unido, con una cerveza
en mano, las pinzas de barbacoa en la otra, boquiabierto, hipnotizado
por sus payasadas escandalosamente arrogantes. Mientras algunes ven la
presidencia de Trump como un ingobernable desastre que se desenvuelve en
cámara lenta justo frente a sus ojos, otres están claramente eufóriques,
animando a Trump como si estuvieran asistiendo a un _rally_ de camiones
monstruo. Trump es un intérprete tan efectivo que hasta ahora no sólo ha
logrado esquivar cualquier repercusión de sus mentiras descaradas, sino
que también está preparado para acusar a los medios establecidos de ser
mentirosos: "Yo tomo mis propias decisiones, en gran parte basadas en
datos acumulados, y todos lo saben. ¡Algunos medios de NOTICIAS FALSAS,
para marginalizar, mienten!" Bajo tal despiadado ataque, la verdad tiene
dificultades para prevalecer.

En contraste con Trump, _Anonymous_ - una cadena de colectivos
desgarbada, semi caótica (aunque a veces bastante organizada), compuesta
por miles de personas y docenas de grupos distintos actuando bajo su
nombre en las cuatro esquinas del globo, con poca a nada de coordinación
entre muchas de ellas - aparece, en todo sentido como una entidad más
seria y digna de confianza. Mientras Trump nos ayuda a ver esto otra
vez, yo he hecho el siguiente punto desde hace mucho tiempo: si une hace
balance de la gran mayoría de sus operaciones después del 2010,
_Anonymous_ por lo general siguió un guión bastante convencional basado
en un impulso por decir la verdad. Anonymous con frecuencia emparejaba
un anuncio sobre alguna indignación que buscaban publicitar con
documentos verificables u otro material. Tal fue la situación cuando
_Anonymous_ lanzó _OpTunisia_ en enero de 2011 y fueron algunas de las
primeras extranjeras en acceder y exhibir ampliamente los vídeos de
protesta que estaban siendo generados en territorio - grabaciones que
publicaron _on line_ para despertar la simpatía pública y estimular la
cobertura mediática. _Anonymous_ de forma rutinaria adquirió correos
electrónicos y documentos (que, por cierto, nunca se ha encontrado que
hayan sido adulterados) y los publicó _on line_, permitiendo que les
periodistas posteriormente los minaran para sus investigaciones. Su
impulso para sacar la verdad a la luz también fue ayudado por material
pomposo ingeniado para que fuera viral. Decir la verdad, después de
todo, siempre se puede beneficiar de una estrategia de relaciones
públicas más astuta.

En ocasiones, _Anonymous_ se basó en el clásico engaño - presionando con
una mentira que en su debido tiempo sería revelada como una mentirilla
para obtener una mayor verdad. Por ejemplo, _LulzSec_ hackeó y
pintarrajeó PBS en represalia por su película de _Frontline_ sobre
_WikiLeaks_, "WikiSecrets", la cual trajo la cólera de miembros de
_LulzSec_ que condenaron la película por cómo sensacionalizó y
psicoanalizó la "oscura" vida interna de Chelsea Manning, eludiendo los
asuntos políticos urgentes planteados por la publicación de cables
diplomáticos de _Wikileaks_. Al ganar acceso al servidor, les hackers
implantaron noticias falsas acerca del paradero de dos raperos célebres.
Con una foto de Tupac Shakur con la cabeza ligeramente ladeada, llevando
una gorra para atrás y una sonrisa cordial, el título anunciaba la
exclusiva: "Tupac sigue vivo en Nueva Zelanda". Continuaba: "El
prominente rapero Tupac ha sido encontrado vivo y bien en un pequeño
complejo turístico en Nueva Zelanda, según reportan los lugareños. El
pequeños pueblo - innombrado debido a riesgos de seguridad -
presuntamente alojó a Tupac y a Biggie Smalls (otro rapero) por varios
años. Un lugareño, David File, murió recientemente, dejando evidencia y
reportes de la visita de Tupac en un diario, que solicitó fuera enviado
a su familia en los Estados Unidos". Aunque de primera entrada podría
parecer poco claro por qué, esta pintarrajeada entregó una declaración
política particularmente potente. Aunque el artículo falso y el hackeo
causaron gran sensación en la prensa global, la mayoría de periodistas
fallaron en abordar la crítica de _LulzSec_ sobre la exagerada
superficialidad de la película. Y aún así, _LulzSec_ logró forzar la
cobertura sensacionalista a través de su combo de hackeo-engaño,
instaurando a través de esta puerta trasera su crítica original acerca
de las tendencias de periodistas a sensacionalizar las noticias.

Pero en la mayoría de casos el engaño fue usado en pequeñas cantidades y
_Anonymous_ simplemente amplificó su mensaje ya siendo transmitido por
otres activistas y periodistas. Por ejemplo, una de sus operaciones más
famosas, _OpSteubenville_, tenía que ver con un horrible caso de asalto
sexual por miembros de un equipo colegial de fútbol en un pequeño pueblo
metalúrgico de Steubenville, Ohio. Luego de que el _New York Times_
escribió una revelación detallando el caso, _Anonymous_ continuó
exhibiendo hiperactivamente la evolución acerca del asalto de
Steubenville a través de vídeos y en Twitter, asegurando su visibilidad
por meses hasta que dos adolescentes fueron encontrados culpables de
violación en marzo de 2013.

_Anonymous_ como Trump, atraían al público y a los medios con actos
ostentosos de espectáculo. Pero _Anonymous_ no se unió como un punto de
voluntad individual para buscar crédito, sino como la convergencia de
una multitud de actorxs contribuyendo a una multitud de movimientos
sociales, colectivos y organizaciones existentes. _Anonymous_ brilló más
intensamente entre 2011 y 2015, durante un periodo tumultuoso de
malestar y descontento global, evidente en el rango de sublevaciones
populares de gran escala a lo largo del mundo: el movimiento 15-M en
España, las primaveras árabe y africana, los campamentos de Occupy, el
movimiento estudiantil en Chile, _Black Lives Matter_, el _Umbrella
Movement_ en Hong Kong. _Anonymous_ contribuyó con cada una de estas
campañas.  Su profundo entrelazamiento con algunas de estas amplias
causas sociales ha sido conmemorado por muches que trabajaron con o se
beneficiaron de _Anonymous_.  En 2011 fue compartida una foto de niñes
tunecines sentades en el patio de su escuela, portando una máscara de
papel blanco de Guy Fawkes, como un gesto de gratitud hacia _Anonymous_
por llevar el mensaje de su situación al mundo.  Más recientemente,
consideré la prematura muerte de Erica Garner, una activista en contra
de la brutalidad policial e hija de Eric Garner, un hombre que murió a
manos de un oficial del departamento de policía de Nueva York. No mucho
después de su muerte, la persona manejando su cuenta de Twitter le
ofreció sus respetos a _Anonymous_: "Un grito para Anonymous... Uno de
los primeros grupos de personas que sujetaron a Erica desde el inicio.
Ella les amaba a todes de verdad #opicantbreathe".

El punto de yuxtaponer las mentiras de Trump con las verdades de
_Anonymous_ es meramente para resaltar que la transparencia y el
anonimato rara vez siguen una fórmula moral binaria, con la primera
siendo buena y la segunda siendo mala. Hay muchos estafadores,
especialmente en la arena política, que hablan y mienten sin una máscara
literal --Donald Trump, Silvio Berluscconi, George W.  Bush, Tony
Blair-- y nunca son obligados a rendir cuentas de forma apropiada, o
requiere de un esfuerzo tipo David y Goliath para eliminarles del poder.
De hecho, Trump, actuando al descubierto, es percibido como
"transparente" porque es un individuo que no se esconde detrás de una
máscara y porque, para algunes, es un político honesto por tener la
valentía de decir cualquier cosa, sin importar qué tan ofensiva sea
(para algunes, entre más ofensiva mejor). Como sugirió el sociólogo
Erving Goffman hace mucho tiempo, los humanos - tan adeptos al arte del
engaño - despliegan un lenguaje astuto y a veces una actuación
confabuladora, en lugar de esconderse, para engañar de forma efectiva.

## Lección 2: el escudo del anonimato

La transparencia se puede lograr a través de marcos institucionales
existentes, ya sea a través del acceso a registros públicos, como usando
la Ley por la Libertad de la información, o usando la función de perro
guardián del Cuarto Estado. Pero cuando estos métodos fallan, les
informantes anónimes pueden ser un mecanismo efectivo para hacer emerger
la verdad. En el caso de la Corte Suprema de 1995 _McIntyre_ Vs. _Ohio
Elections Commission_ se articula de forma convincente el apoyo a esta
posición que argumenta que el anonimato resguarda a le votante, la
persona honesta, e incluso a le opinadore anónime de la venganza
gubernamental o las masas enojadas del cuerpo político. Les jueces de
dicho caso escribieron: "El anonimato es un escudo de la tiranía de la
mayoría.... Así ejemplifica en particular el propósito detrás de la
Carta de Derechos de la Primera Enmienda: para proteger a individuos
impopulares de represalias... a manos de una sociedad intolerante". Para
señalar su conciencia de y contribución a esta tradición, les
participantes de _Anonymous_ gustan de citar a Oscar Wilde: "Una persona
es menos sí misma cuando habla en su propia persona. Denle una mascara y
les dirá la verdad".

Uno de los ejemplos más impactantes y efectivos que confirma el
fundamento de la _Corte Suprema_ y del aforismo de Oscar Wilde involucra
una máscara facial portada por un doctor en medicina. En 1972 un
psiquiatra presentando en una reunión de la _Asociación Americana de
Psiquiatría_ se escondió a sí mismo con un distorsionador de voz, un
nombre seudónimo y una máscara de hule.  Llamándose Dr. H. Anonymous, y
en servicio en un panel llamado "Psiquiatría: ¿Amiga o enemiga de los
homosexuales?", el doctor empezó confesando: "Soy homosexual. Soy
psiquiatra". En esa época la homosexualidad había sido clasificada por
la psiquiatría como una enfermedad, haciéndola particularmente inmune a
las críticas. Esta revelación audaz y valiente logró lo que Dr. H.
Anonymous y sus aliades se habían propuesto: envalentonar los esfuerzos
en curso para despatologizar la homosexualidad. Solo un año más tarde,
la asociación había removido la homosexualidad de su manual de
diagnóstico y el Dr. H. Anonymous, quien había temido que no recibiría
la plaza académica si su empleador se enteraba que era gay, permaneció
protegido (y con empleo), y sólo hizo su nombre público veintidós años
más tarde, John E. Fryer.

Muchas otras personas y grupos han hablado y actuado honestamente
encubiertes en un intento de exponer algún abuso o crimen y usar el
anonimato para escudarse a si mismes no solo de pares, colegas o
empleadores, como hizo Dr.  Fryer, pero también de retribución
gubernamental. _Anonymous_, _Antifa_, Chelsea Manning (durante su corta
ocupación como filtradora anónima), _Garganta Profunda_ (la fuente
anónima durante el escándalo de _Watergate_), y la _Comisión Ciudadana
para Investigar al FBI_ - de las cuales todas han ganado alguna medida
de respeto por sus palabras y acciones, no por sus identidades legales -
han entregado una transparencia que ha sido considerada valiosa sin
importar su percibida falta impunidad u opacidad. En la exposición de
delitos atroces del gobierno, el anonimato tiene el potencial de hacer
que el acto riesgoso de la denuncia sea un poco más seguro. Tal fue el
caso de la _Comisión Ciudadana para Investigar al FBI_, un grupo de ocho
paladinxs opuestes a la guerra que irrumpieron en una oficina del FBI en
1971 y se llevaron cajones de archivos que contenían pruebas de
_COINTELPRO_, un programa secreto de vigilancia y desinformación
impuesto contra docenas de movimientos activistas. El programa fue
eventualmente cerrado luego de ser considerado ilegal por el gobierno de
los Estados Unidos y les intruses nunca fueron detenides. Si estes
ciudadanes hubiesen sido atrapades - el FBI dedicó doscientas agentes al
caso pero, fallaron en encontrar tan siquiera a une de les intruses, se
rindió en 1976 - su destino hubiese probablemente incluido una costosa
batalla legal seguida por un buen tiempo detrás de los barrotes.

Trágicamente, la gente que ha hablado descubierta a veces ha sido
expuesta a daños y calumnias. Ser honeste y transparente, en especial
cuando le faltan partidaries y fieles, nos pone en riesgo de una
traumática pérdida de privacidad y como en el caso de Chelsea Manning,
de seguridad física. Después de ser expuesta por un hacker, Manning fue
torturada por un año en confinamiento solitario debido a su denuncia. La
ex gimnasta estadounidense Rachael Danhollander, una de las primeras que
se atrevió en denunciar a Larry Nassar, el médico del equipo de
gimnastas olímpicas de Estados Unidos que asaltó sexualmente a más de
260 mujeres jóvenes, explicó en un editorial que su vida y reputación
fueron arruinadas por hablar, hasta que la marea empezó a cambiar:
"Perdí mi iglesia.  Perdí a mis amigas más cercanas como resultado de
abogar por las sobrevivientes que han sido victimizadas por fallos
institucionales similares en mi propia comunidad.  Perdí cada fragmento
de privacidad". Todos estos ejemplos traen a la mente el adagio
"privacidad para les débiles, transparencia para los poderosos". El
anonimato puede llenar una prescripción de transparencia al proteger de
represalias a las honestas.

## Lección 3: Contención del ego y daños de la celebridad desenfrenada

El rechazo de _Anonymous_ a los cultos de la personalidad y a la
búsqueda de celebridad es el impulsor menos entendido del anonimato,
pero uno de los más vitales de entender. Los trabajos del anonimato bajo
este registro funcionan menos como un dispositivo de honestidad y más
como un evento de nivelación social. A menos que siguieran a _Anonymous_
de cerca, este _ethos_ era más difícil de deducir, ya que sólo era
notablemente visible en los canales traseros de sus interacciones
sociales - en salas de chat privadas o semi privadas con explosiones
ocasionales en Twitter, tales como este tweet de \@FemAnonFatal:

> • FemAnonFatal es un Colectivo • NO un movimiento individual No un
> lugar para autopromoción NO un lugar para el ODIO SINO un lugar para
> la SORORIDAD Es un lugar para Nutrir la Revolución Lea Nuestro
> Manifiesto... • Deberían habernos previsto • \#FemAnonFatal
> \#OpFemaleSec

Por supuesto, es más fácil proferir tales pronunciamientos elevados
sobre solidaridad que implementarlos. Pero _Anonymous_ imponía este
estándar castigando a quienes salían a la luz buscando fama y crédito.
En mis muchos años de observarles, fui testiga directa de las
consecuencias para aquellas que violaron esta norma. Si une participante
novatx era viste fijándose demasiado en obtener elogios de sus pares,
elle podría ser reprendide suavemente. Para aquelles que se atrevieron a
adjuntar sus nombres legales a alguna acción o creación, la restitución
era más feroz. Como mínimo, le transgresore era usualmente ridiculizade
o fustigade, con unos pocos individuos ritualmente "asesinados" al
prohibirseles participar en una sala de chat o red.

Junto a momentos salpicados de acción disciplinaria, esta norma tendía a
ser tarareada por lo bajo en el fondo, pero no de forma menos poderosa
--mandando que todo lo creado bajo la tutela de _Anonymous_ fuese
atribuido al colectivo. Es importante afirmar que en contraste con sus
más conocidos compatriotas hackers y forajidos, la mayoría de les
participantes de _Anonymous_ estaban maniobrando en un territorio legal
sin ambigüedades; aquelles que conjuraban un mensaje convincente de
esperanza, disidencia o protesta a través de medios como vídeos,
manifiestos enérgicos, imágenes u otras astutas llamadas a las armas
ingeniadas para volverse virales, no estaban incentivadas hacia el
anonimato por el castigo legal. Es más, el decreto ético de sublimar la
identidad personal tenía dientes: les participantes por lo general se
abstenían de firmar con sus nombres legales estos trabajos, algunos de
los cuales llegaron a la prominencia, recibiendo cientos de miles de
vistas en _YouTube_. Mientras une recién llegade se hubiera rendido ante
este decreto por miedo al castigo, la mayoría de les participantes
llegaron a abrazar este _ethos_ como una estrategia necesaria para los
objetivos más amplios de minimizar la jerarquía humana y maximizar la
equidad.

Observar esta contención del ego fue revelador. La pura dificultad de
vivir este credo se reveló en la práctica. Como antropóloga, mi labor
metodológica manda cierto grado de participación directa. La mayoría de
mi labor con _Anonymous_ consistió en trabajo de traducción
periodística, pero en unas pocas ocasiones me uní a un pequeño grupo de
creadorxs mediáticxs para fabricar mensajes incisivos para vídeos
diseñados con el fin de provocar que la gente tomara acción. Como una
escritora académica separada de la necesidad de concisión, recuerdo
brillar con orgullo por la redacción compacta que improvisé para
canalizar la furia colectiva acerca de alguna asquerosa injusticia
política.  Resistir incluso un poquito de crédito por la hazaña fue
difícil en ese momento, pero a la larga fue satisfactorio, sentando las
bases para hacerlo de nuevo.  Aun así, no solo iba en contra de lo que
me había enseñado la sociedad, sino que también del modo de ser
académica - alguien cuyo sustento depende enteramente de un sistema bien
arraigado con siglos de antigüedad que asigna respeto con base en el
reconocimiento individual. Como la auto nombrada autora de esta pieza,
sería hipócrita abogar por una moratoria total a la atribución personal.
Pero cuando una economía moral que está basada en el impulso por el
reconocimiento individual se expande de tal manera que desplaza otras
posibilidades, podemos descuidar, para nuestro riesgo colectivo, otras
formas esenciales de ser y estar con otres.

Uno de los muchos peligros del individualismo o la celebridad sin
control es la facilidad con la que se transforma en narcisismo, un rasgo
de la personalidad que obviamente excluye la ayuda mutua, ya que
prácticamente garantiza algún nivel de caos interpersonal, cuando no la
total carnicería en forma de vitriolo, matonismo, intimidación y
mentiras patológicas. Trump, de nuevo, puede servir como una referencia
útil, ya que viene a representar un ideal casi platónico de narcisismo
en acción. Su presidencia ha demostrado que un solipsismo sin complejos
puede actuar como un tipo de lente distorsionador, previniendo el
funcionamiento normal de la transparencia, la verdad, el avergonzamiento
y la rendición de cuentas al ofrecer una indiferencia tan completa que
parece casi incapaz de contemplar la difícil situación de otras o
admitir equivocación. Y en el ascenso de Trump yace una lección mucho
más perturbadora y general que contemplar: que obtener una de las
posiciones políticas más poderosas en una de las naciones más poderosas
del mundo es posible sólo porque tales comportamientos en búsqueda de la
celebridad son recompensados en muchos aspectos de nuestra sociedad.
Muchos ideales culturales dominantes nos exigen buscar reconocimiento
--ya sea de nuestras acciones, palabras o imágenes. Aunque la celebridad
como un ideal no es nueva de ninguna forma, en internet hay
proliferantes avenidas sin fin a nuestra disposición para darse cuenta,
registrar de forma numérica (en _likes_ y _retweets_), y así consolidar
y normalizar más la fama como una condición de cotidianeidad.

Desde luego, el narcisismo y la celebridad están lejos de estar sin
control. Por ejemplo, los rasgos vanidosos y auto engrandecedores de
Trump hoy son sujetos de una crítica y análisis salvajes por una
camarilla de expertes, periodistas y otres comentaristas. Incluso si la
celebridad es un ideal cultural duradero persistente y siempre en
expansión, la humildad también es valorada. Esto es más obviamente
cierto en la vida religiosa, pero una bandada de prohibiciones éticas y
mundanas diarias, también buscan frenar el apetito del ego humano por
gloria y gratificación. Algo tan menor como la sección de
agradecimientos de un libro sirve --aunque sea mínimamente-- para
refrenar la noción egoísta de que les individues son enteramente
responsables de las loables creaciones, descubrimientos y/o trabajos
artísticos que se les atribuyen.  Después de todo es la extendida
confesión y el momento de gratitud para reconocer que tal escrito
hubiese sido imposible, o mucho peor, sin la ayuda de una comunidad de
pares, amistades y familiares. Pero historias que celebran la
solidaridad, equidad, la ayuda mutua y la humildad son más raras. Y más
escasos aún son los mandatos en los que les individues son llamades a
perfeccionar el arte de la auto obliteración. _Anonymous_ es
probablemente uno de los laboratorios más grandes y abierto a muches, en
llevar a cabo un experimento colectivo para limitar el deseo del crédito
individual, fomentando formas de conectar con nuestros pares a través de
compromisos de indivisibilidad.

Aunque el anonimato puede incentivar todo tipo de acciones y
comportamientos, en el caso de _Anonymous_ significó que muches de les
participantes activaran por razones de principio. Su misión de
principios íntegros para corregir los errores infligidos a personas
encarna el espíritu del altruismo. Su demanda por humildad ayuda a
desalentar, incluso si no eliminó del todo, a aquelles participantes que
simplemente codician la gloria personal al unirse a las filas del grupo.
Voluntaries, obligades a dar crédito a _Anonymous_, también mantuvieron
bajo control un problema que azota todo tipo de movimientos sociales: la
auto nominación de estrella de rock o líder, propulsada al estrellato
por los medios, de quienes éxitos o fallos de reputación pueden servir
injustamente como representantes del ascenso y caída del movimiento de
manera más general. Si tal auto promoción se vuelve flagrante, los
conflictos y las luchas internas típicamente afligen las dinámicas
sociales, que a su vez debilitan el poder del grupo para organizarse de
forma efectiva. La ya limitada energía se desvía de las campañas y en su
lugar se gasta en manejar individuos hambrientos de poder.

Es peligroso romantizar el anonimato como virtuoso por sí mismo. El
anonimato _on line_, combinado con masculinidades patriarcales
--abusadores patológicos, criminales y hordas colectivas de troles--
hace posible comportamientos con consecuencias espantosas y a veces, de
hecho realmente aterradoras. El anonimato puede ayudar e incitar a la
crueldad tanto como puede engendrar fines morales y políticos más nobles
--depende del contexto. Hacer un balance de la historia completa de
_Anonymous_ ilustra esta dualidad. Antes de 2008 el nombre _Anonymous_
había sido usado casi de forma exclusiva para el propósito del
_trolling_ en internet - una práctica que con frecuencia significa
apuntar a personas y organizaciones para acosarles, profanar sus
reputaciones y revelar información personal humillante. Habiendo sido
apuntada en 2010 por un ataque de troles (por suerte fallido), estaba
emocionada - aunque bastante sorprendida - del dramático proceso de
conversión que experimentó _Anonymous_ entre 2008 y 2010 conforme
empezaron a trolear a los poderosos, eventualmente combinando la
práctica con vocabularios y repertorios de protesta y disidencia más
tradicionales.

Conforme se separaron de troles puros, lo que quedó igual fue el
compromiso al anonimato, usado para fines diferentes en circunstancias
diferentes. Aún así, una cantidad de operaciones de _Anonymous_ que
sirvieron al interés público, tales como la violación masiva de la
privacidad de las personas mediante la difusión de de información
asociada a sus correos electrónicos, fueron llevados a cabo de formas
cuestionables y son dignos de condena. Estas operaciones imperfectas no
deberían anular los aspectos positivos que el grupo logró a través del
anonimato, pero no obstante deberían ser criticados por sus violaciones
a la privacidad y usados como ejemplos para mejorar sus métodos.

Prevenir que el estado erradique el anonimato requiere fuertes razones
por su rol esencial en salvaguardar la democracia. Al defender el
anonimato es difícil simplemente argumentar, mucho menos probar, que el
bien que posibilita pesa más que sus daños, ya que es difícil hacer
recuento del resultado social del anonimato. A pesar de las dificultades
de medición, la historia ha mostrado que los estados-nación con poder de
vigilancia sin control derivan hacia el despotismo y el totalitarismo.
Les ciudadanes bajo vigilancia o simplemente bajo amenaza de vigilancia,
viven con miedo de retribución y están desalentades a decir lo que
piensan, organizarse y romper la ley individualmente, de formas que
mantengan a los estados y corporaciones responsables de sus actos.

Sin equivocación, defender el anonimato de tal forma no hace aceptables
todos los usos del anonimato por parte de les ciudadanes. Cuando
evaluamos la vida social del anonimato, une debe también hacer una serie
de preguntas: ¿Qué es la acción anónima? ¿Cuáles personas, causas o
movimientos sociales están siendo ayudadas? ¿Es hacia arriba o hacia
abajo? Todos estos factores clarifican lo que está en juego y las
consecuencias de usar el escudo del anonimato. Invita a soluciones para
mitigar algunos de sus daños en lugar de demandar la eliminación del
anonimato totalmente. Les tecnólogues pueden rediseñar plataformas
digitales para prevenir abusos, por ejemplo, habilitando el reporte de
cuentas ofensivas. Pero reconocer el mal uso del anonimato es la razón
por la que también aseguramos la aplicación de una normativa de
capacidades limitadas para desanonimizar a aquellos que están usando una
cubierta para actividades que la sociedad considera inadmisibles, como
la pornografía infantil por ejemplo.  En su forma actual el estado
dirige enormes recursos, en forma de dinero, tecnología y legitimidad,
para una aplicación efectiva de la ley. Además de esto, llamar a acabar
el cifrado fuerte, agregar puertas traseras para el acceso
gubernamental, o prohibir las herramientas de anonimato --algo que el
FBI hace con frecuencia-- es llamar a la eliminación inaceptable de
muchos usos legítimos del anonimato.

A pesar de estas justificaciones, es difícil defender el anonimato
cuando algunas personas solo tienen un rudimentario sentido de la
conexión de éste con los procesos democráticos, o que no ven del todo la
necesidad, mientras que otres lo ven solamente como un imán para formas
depravadas de criminalidad, cobardía y crueldad. Recientemente me
recordaron este punto luego de encontrarme con una de mis anteriores
estudiantes mientras viajaba. Sorprendida de reconocerme en el grupo con
el que estaba a punto de bucear, ella con regocijo me identificó por el
sujeto de estudio: "¡Usted es la profesora hacker!" Unas horas después,
cuando salíamos de un pequeño bote, ella me preguntó de forma espontánea
que le recordara mis argumentos en contra de la común desestimación de
la privacidad y el anonimato por razones de que quien habla "no tiene
nada que esconder". Yo solté una risita, ya que mi mente estaba ocupada
con estas mismas preguntas porque estaba preocupada por este artículo y
recité algunos argumentos aquí explorados. No estoy segura si los
argumentos precisos se le olvidaron porque pasaron años, porque mi
lección fue aburrida o porque los méritos del anonimato son
contraintuitivos para muches; es probable que haya sido una combinación
de las tres. De cualquier forma, estaba contenta de que ella tan
siquiera tuviera la pregunta en su mente.

Fue un recordatorio de que en un momento en el que los ejemplos de
actorxs anónimes trabajando por el bien no están fácilmente disponibles
en las noticias, tal como sucedió durante los días de _Anonymous_;
aquelles de nosotres tratando de salvar la reputación del anonimato
debemos presentar historias convincentes de bien moral posibilitadas por
el anonimato, en lugar de explorarlo sólo como un concepto abstracto,
justo por si mismo, independiente del contexto. _Anonymous_ permanece
como un caso de estudio ejemplar con ese objetivo. Aparte de usar el
escudo para la acción directa y disidencia para buscar la verdad y
transparencia, _Anonymous_ también ha brindado una zona en la que la
recalibración del crédito y la atribución no sólo han sido discutidas,
sino realmente promulgadas. Al hacerlo, _Anonymous_ brindó asilo de la
necesidad incesante de disputa por la atención personal, haciéndose
notorias a la vez que atenuaban la celebridad individual, y aún así
lograron combatir la injusticia con espectáculo, mientras se mantenían
anónimes como unx solx.
