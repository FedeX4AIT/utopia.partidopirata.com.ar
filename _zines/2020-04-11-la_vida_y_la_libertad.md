---
author: Nicky Case
title: Proteger la vida y la libertad
layout: post
signature: 0
toc: true
original: "https://ncase.me/contact-tracing/"
---

![La vida y la libertad 1](assets/images/la_vida_y_la_libertad/panel0001.png)

![La vida y la libertad 2](assets/images/la_vida_y_la_libertad/panel0002.png)

![La vida y la libertad 3](assets/images/la_vida_y_la_libertad/panel0003.png)

![La vida y la libertad 4](assets/images/la_vida_y_la_libertad/panel0004.png)

![La vida y la libertad 5](assets/images/la_vida_y_la_libertad/panel0005.png)

![La vida y la libertad 6](assets/images/la_vida_y_la_libertad/panel0006.png)

![La vida y la libertad 7](assets/images/la_vida_y_la_libertad/panel0007.png)

![La vida y la libertad 8](assets/images/la_vida_y_la_libertad/panel0008.png)

![La vida y la libertad 9](assets/images/la_vida_y_la_libertad/panel0009.png)

![La vida y la libertad 10](assets/images/la_vida_y_la_libertad/panel0010.png)

![La vida y la libertad 11](assets/images/la_vida_y_la_libertad/panel0011.png)

![La vida y la libertad 12](assets/images/la_vida_y_la_libertad/panel0012.png)

![La vida y la libertad 13](assets/images/la_vida_y_la_libertad/panel0013.png)

![La vida y la libertad 14](assets/images/la_vida_y_la_libertad/panel0014.png)

![La vida y la libertad 15](assets/images/la_vida_y_la_libertad/panel0015.png)

![La vida y la libertad 16](assets/images/la_vida_y_la_libertad/panel0016.png)

![La vida y la libertad 17](assets/images/la_vida_y_la_libertad/panel0017.png)

![La vida y la libertad 18](assets/images/la_vida_y_la_libertad/panel0018.png)

