---
title: 'Error 404: democracia en los partidos digitales no encontrada'
author: Julia Rone
layout: post
toc: false
signature: 0
---

Error 404:
==========

Democracia en los partidos digitales no encontrada
--------------------------------------------------

> Esta es una traducción pirata del artículo "[Error 404: digital party
> democracy not
> found](https://roarmag.org/essays/error-404-digital-party-democracy-not-found/)"
> publicado el 18 de febrero de 2019 en [ROAR](https://roarmag.org/).

Esto es una reseña del libro de Paolo Gerbaudo, "El partido digital: La
organización política y la democracia online" (_Pluto Press_, 2018).

En la década de 1960 el activista estudiantil alemán Rudi Dutschke
propuso el concepto de "la larga marcha a través de las instituciones",
descrito por Herbert Marcuse como una estrategia para trabajar "en
contra de las instituciones hegemónicas, pero desde su interior". No
sólo se refería a subvertir el orden existente, sino a algo mucho más
elegante: cumplir debidamente con el trabajo propio e ir aprendiendo
cómo funcionan las instituciones mientras se mantiene una posición
crítica. Para poder cambiar las reglas del juego, primero tendríamos que
dominarlas.

Pero como señaló el académico en movimientos sociales David Mayer, el
problema es que transitar las instituciones usualmente transforma más a
quienes las transitan que a las instituciones mismas.

Cincuenta años después, transformar la indignación radical en cambios
positivos no es menos desafiante. Como consecuencia de la Gran Recesión
del 2008, les activistas tuvieron una visión tecno-utópica, una novedosa
y valiente idea sobre cómo institucionalizarse preservando al mismo
tiempo el impulso crítico y la inclusividad democrática.

La solución fue "actualizar" y democratizar radicalmente el propio
concepto de partido a través del uso de medios digitales y plataformas
para toma de decisiones en línea. Siguiendo el ejemplo del software
_Liquid Feedback_ de algunos _Partidos Piratas_, el movimiento _Cinco
Estrellas_ de Italia adoptó una plataforma propia llamada _Rousseau_,
mientras que _Podemos_ creó _Participa_ en España.

Pero estos "partidos digitales", ¿realmente se volvieron más
democráticos? ¿"Transitar las plataformas" no ha creado su propia
distribución desigual de poder? El último libro de Paolo Gerbaudo
(profesor en el _King's College_ de Londres), _The Digital Party_ \["El
partido digital"\], plantea esta pregunta sobre la base de 30
entrevistas a activistas, expertes y un análisis abarcativo de las
publicaciones de los partidos.

_The Digital Party_ actualiza el trabajo previo de Gerbaudo sobre las
relaciones de poder en el movimiento de las plazas del cual han
re-emergido algunos partidos políticos. Entrando en diálogo con el nuevo
libro de Della Porta y colaboradores, _Movement parties against
austerity_ \["Movimientos partidarios contra la austeridad"\], Gerbaudo
analiza el surgimiento y estructura organizacional de los partidos
digitales populistas, incluyendo los partidos piratas de Alemania y
Suecia, el _Movimiento Cinco Estrellas_ italiano, _Podemos_ de España,
_La France Insoumise_ \[La Francia insumisa\] y las campañas digitales
del Partido Laborista inglés y de Bernie Sanders en Estados Unidos.

En tiempos donde les politologues despotricaban sobre la apatía
política, el partido político tuvo un retorno inesperado mediante una
transformación radical. Como han demostrado Simon Tormey y Ramón
Feenstra y a pesar de una fuerte crítica a los partidos políticos que se
impuso en el discurso social, en España se registraron 295 partidos
políticos entre 2009 y 2010, número que casi se duplicó en el período de
protestas que ocurrieron en todo el país entre 2011 y 2012.

Aún así es válido afirmar que salvo les expertes en política española,
nadie se enteró de esos partidos. Diferente es el caso de los partidos
digitales que se las arreglaron para emerger y ganar cientos de miembres
en muy poco tiempo con su propuesta tecno-populista. ¿Cómo lo hicieron?

El surgimiento de los partidos digitales refleja un quiebre político en
respuesta a dos hechos diferentes pero entrelazados: la recesión de 2008
y la revolución digital. El quiebre es entre les _insiders_ de la
política/economía y les _outsiders_ conectades, personas con estudios y
acceso Internet por arriba del promedio que se ve en dificultades
económicas, condiciones precarias de trabajo, desempleo, bajos salarios
y generalmente con cierta sensación de alienación del sistema político y
sus formas de representación.

Esta gente jóven, educada y a menudo en bancarrota encontró en los
partidos digitales un canal para darle voz a sus reclamos sobre derechos
digitales (privacidad y transparencia ante todo), democracia real y
justicia económica; reclamos que los principales partidos políticos
fallaron en dar respuesta y que se han convertido en la base del éxito
electoral de los partidos digitales.

Desde una perspectiva histórica más amplia, Gerbaudo muestra cómo los
partidos digitales difieren de los partidos industriales masivos de la
era fordista y de los partidos neoliberales de finales de los '90. Hace
un paralelismo entre la forma principal de organización partidaria y la
forma de producción dominante, siendo los partidos digitales la
respuesta lógica al surgimiento del capitalismo de la plataforma
dominado por los gigantes monopólicos como _Facebook_, _Google_ y
_Amazon_ entre otros.

El partido digital arquetípico se parece bastante a las plataformas
en línea. En primer lugar, promueve un modelo de membresía similar al
proceso de registro que requieren _Facebook_ o cualquier otro gigante de
la web. Además, el partido digital reune información política y descansa
sobre la base del trabajo gratuito de sus miembres, de forma similar a
cómo las plataformas en línea dependen de nuestras contribuciones y
participación para funcionar. El partido digital parece democrático y
habla sobre la democracia, pero al final de cuentas es _Facebook_
disfrazado.

El partido digital tiene dos aspectos al mismo tiempo, uno destructivo y
otro constructivo. En el capítulo _Death of the Party Cadre_ \["La
muerte de los cuadros partidarios"\], Gerbaudo analiza la notable
transformación que llevan a cabo los partidos digitales al deshacerse de
--o más bien, no molestarse en instituir-- atributos clásicos del
partido como tener sedes centrales altamente simbólicas, células locales
y espacios físicos para reunirse en persona.

En lugar de eso, les activistas trabajan a distancia, desde sus casas,
en bares, siempre en movimiento. Se trata de una dinámica similar a la
que se dio en _Silicon Valley_ y _San Francisco_, donde el trabajo se
tercerizó a distintos espacios de trabajo: la burocracia del partido es
sustituída por una micro-burocracia distribuida --una barcracia,
podríamos decir-- que está en todos lados y en ningún lugar a la vez.

La participación se glorifica, pero es una forma de participación
altamente individualista que se ajusta perfectamente a la desconfianza
neoliberal hacia las formas de organización "no espontáneas". El partido
se virtualiza y su antigua estructura organizacional se transforma en un
proceso.

La "migración"[^2] a plataformas digitales en línea, constituye lo que
Gerbaudo llama la parte constructiva de los partidos digitales.  Sin
embargo, esta promesa de que las plataformas garantizan una
participación más abierta e inclusivo se han quedado cortas.

[^2]: Se habla de migración en términos técnicos cuando se da un proceso
  de adopción de tecnologías que reemplazan a otras u otras prácticas,
  por ejemplo, se migra de Microsoft Windows a GNU/Linux (nota de la
  traducción).

Las plataformas no son intermediarias neutrales y objetivas. Las reglas
y las relaciones de poder están codificadas en el mismo software que
usamos.

Por ejemplo, a diferencia de Podemos la plataforma de participación del
_Movimiento Cinco Estrellas_, _Rousseau_, es una ramificación de
software propietario[^3] que es particularmente vulnerable. Gerbaudo
narra un incidente impactante que se dio en 2017 cuando une hacker "de
sombrero negro"[^4] logró acceder y descargar información personal de
les miembres de _Rousseau_ y la puso en venta por 0,3 Bitcoin, que en
aquel momento equivalían a 1000 Euros.

[^3]: Se dice software propietario al software que no es libre, que no
  se puede estudiar ni modificar libremente.

[^4]: Les _hackers_ que ingresan a sistemas ajenos distinguen sus
  actividades según el color de su sombrero.  Si es negro, el ingreso
  puede ser malicioso (aunque justo, según el caso...), si es blanco, el
  ingreso se hace con la voluntad de ayudar a la entidad atacada a
  mejorar sus prácticas de seguridad y si es gris, se trata de una
  mezcla entre ambos colores.

Probablemente la mayor contradicción entre las ideologías de la web y la
praxis de los partidos digitales yace el predominio de un tipo de
democracia plebiscitaria en línea, a expensas de otros dos modelos de
democracia, el representativo y el deliberativo.  El uso más prominente
de las plataformas partidarias en línea es el de votar por sí o por no
las propuestas que hace el liderazgo del partido.

Les líderes de los partidos digitales tienen poder sobre la elección del
momento para impulsar propuestas, su contenido y encuadre. Incluso
pueden ignorar los resultados de una votación pública cuando éstos no
les gustan. Pero esto último raramente es necesario dado que la mayoría
de las votaciones en las plataformas digitales resultan a favor de las
propuestas de los cuadros.

Así, el desmantelamiento de las viejas burocracias y formas de mediación
se encontraron mano a mano con una centralización inesperada de poder.
"Hiperlíderes" con pinta de celebridades, figuras carismáticas como
Pablo Iglesias o Bernie Sanders, encontraron el reflejo de su imagen en
la "super base", un conjunto de seguidores fieles que son miembres del
partido, que reaccionan al mensaje del lider, votan, _laikean_ y
comparten en línea, en lo que podría pensarse como una forma de
democracia "reactiva".

Como Gerbaudo señala aforísticamente: "Parece que dejamos la ley de
hierro de la oligarquía solamente para darnos de frente contra la 'ley
de silicona' del 'dictador benevolente'".[^4] El partido digital
actualizó la forma del partido tradicional, pero en el proceso se
perdieron un montón de cosas. Esto no significa sin embargo que debamos
volver a la vieja versión del partido. Gerbaudo hace una serie de
propuestas sobre cómo mejorar la herramienta partidaria digital. En
primer lugar, les miembres de base tienen que tener más iniciativa y
voz. Aun más, la administración de las plataformas en línea tienen que
ser separada de les referentes del partido para evitar su manipulación.
Los partidos digitales también tienen que encontrarse regularmente en
espacios físicos desconectados para promover distintas formas de
integración social.

Por último, los partidos digitales se han enfocado tanto en reformar su
composición orgánica que descuidaron el contenido de sus propuestas.
Estos partidos tienen que abandonar la ilusión de que pueden representar
a todes y enfocarse en desarrollar un programa político coherente, o
dicho de modo tradicional, una plataforma. Por suerte es una tarea
factible aunque difícil.

[^4]: La silicona hace referencia al material con el que están hechos
  los componentes informáticos (de ahí el mote _Silicon Valley_) y la
  "dictadura benevolente" es un principio de gobierno de proyectos de
  software libre donde la persona (usualmente un varón cis) que inició
  el proyecto tiene poder de veto frente al desarrollo del proyecto,
  aunque acepte colaboraciones de muches participantes.  Esto claramente
  da lugar a situaciones abusivas, por ejemplo, Linus Torvalds es el
  "dictador benevolente" de Linux y tuvo el privilegio de abusar y
  menospreciar a miles de colaboradores durante años.

El trabajo de Gerbaudo abre nuevas interrogantes: ¿Cómo los partidos
digitales se adecúan a distintos sistemas partidarios nacionales y cómo
su éxito es influenciado por la fuerza relativa de apoyo de otros
partidos? ¿Por qué el Partido Pirata en Alemania fracasó tan
espectacularmente, mientras que el _Movimiento Cinco Estrellas_ está
ahora formando una colación de gobierno en Italia?

Otro problema a explorar son las interacciones entre los partidos y los
movimientos sociales, ¿cómo los partidos digitales respondieron a las
exigencias de los movimientos sociales?

En tercer lugar, ¿los partidos digitales tuvieron logros importantes en
Europa del Este debido al alto nivel de digitalidad de las problemáticas
allí? Si no, ¿por qué?

Aunque _The Digital Party_ se haya enfocado en los movimientos
progresistas europeos, resulta altamente importante examinar las
múltiples formas en que las que la ultra-derecha ha hecho uso de los
medios digitales y ha cooptado prácticas "progresistas" como las firmas
y el financiamiento colectivo.  La tipología y análisis que provee
Gerbaudo son una base sólida para profundizar en estas cuestiones.

En última instancia, _The Digital Party_ es una obra ambiciosa,
imaginativa y de amplia escala que no duda en hacer grandes preguntas
mientras esquiva el peligro más importante para las ciencias sociales,
es decir el foco en problemas específicos que solo preocupan a un
pequeño círculo académico.

Sería justo decir que esta es una obra tecno-escéptica, aunque nunca
alarmista ni pesimista.  Es un relato cuidadoso y razonado de las
transformaciones tecno-políticos que están teniendo lugar en frente
nuestro.  En un sentido, Gerbaudo es como un "hacker de sombrero blanco"
político, que revela las fallas en el modo de operación de los partidos
digitales y muestra cómo pueden ser reparados.  Esperemos que nos llegue
su actualización.
