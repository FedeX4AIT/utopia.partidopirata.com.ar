---
author: CrimethInc.
title: Desertando de la utopía digital
layout: post
signature: 0
toc: true
original: "https://es.crimethinc.com/2013/10/04/feature-deserting-the-digital-utopia"
---

Las Computadoras contra la computación
--------------------------------------

> _Existe un mundo invisible conectado al mango de cada herramienta
> --cuando usamos la herramienta para lo que se la diseñó, entramos en
> el molde de quienes hacen lo mismo. Pero si desconectamos la
> herramienta de ese mundo podemos lanzarnos a mapear otros_.
> [Hunter/Gatherer](https://crimethinc.com/journals/hunter-gatherer/1)

El producto capitalista ideal realizaría su valor del constante trabajo
impago de la especie humana.  Nosotres seríamos prescindibles, pero el
producto sería indispensable.  Integraría la actividad humana por
completo en un solo terreno unificado, accesible solo a través de
productos corporativos adicionales, donde mercado y taller clandestino
estarían combinados. Lograría todo esto bajo la bandera de la autonomía
y la decentralización, e incluso quizás de la "democracia directa".

Seguramente cuando tal producto fuera inventado, algunes
anti-capitalistas bien-intencionades proclamarían que el reino de los
cielos se acerca --restaría quitar al capitalismo de la ecuación. El
himno de los lotófagos[^desertando-lotofagos].

[^desertando-lotofagos]: En la Odisea, los lotófagos se alimentaban de
la flor de loto cuya dulzura les hacía olvidar todo el sufrimiento
vivido en el pasado (nota de la traducción).

No sería la primera vez que les disidentes extrapolásemos nuestra utopía
de la infraestructura del orden dominante.  ¡Recordemos el entusiamo de
Karl Marx y Ayn Rand por las vías de tren!  En contraste, creemos que la
tecnología producida por la competencia capitalista tiende a encarnar
e imponer su lógica y si queremos escapar de este orden, nunca debemos
tomar las herramientas por dadas.  _Cuando usamos herramientas, somos
usades por ellas a su vez_.

A continuación exponemos nuestro intento por identificar la ideología
incorporada en la tecnología digital y enmarcar algunas hipótesis sobre
cómo abordarla.

La red se cierra
----------------

En nuestra época, la dominación no es solo impuesta por órdenes emitidas
de gobernantes a gobernades, sino por algoritmos que sistemáticamente
producen y constantemente recalibran diferenciales de poder.  El
algoritmo es el mecanismo fundamental que perpetúa las jerarquías
actuales; determina las posibilidades de antemano, a la vez que ofrece
una ilusión de libertad como elección.  Lo digital reduce las infinitas
posibilidades de la vida a una red de algoritmos interconectados --a
elecciones entre ceros y unos. El mundo se reduce a la representación
y las representaciones se expanden para llenarlo; lo irreductible
desaparece. Aquello que *no computa* no existe. Lo digital puede
presentar un impresionante conjunto de elecciones --de posibles
combinaciones de unos y ceros-- pero los términos de cada elección están
pre-establecidos.

Una computadora es una máquina que realiza algoritmos.  Originalmente,
el término designaba a una persona humana[^desertando-computadora] que
seguía órdenes tan estrictamente como una máquina.  Alan Turing, el
patriarca de la ciencia de la computación, bautizó a la computadora
digital en una extensión metafórica de la forma más impersonal de
trabajo humano: "La idea detrás de las computadoras digitales es que
estas máquinas puedan realizar cualquier operación de la que sea capaz
una computadora humana".  Cincuenta años después, esta metáfora ha sido
invertida una y otra vez, a medida que humanes y máquinas se volvieron
cada vez más indivisibles.  "Se espera que la computadora humana siga
reglas fijas --continuó Turing-- sin autoridad para desviarse en ningún
detalle"[^desertando-turing].

[^desertando-computadora]: La computación era un trabajo que realizaban
  cis-mujeres, resolviendo --computando-- algoritmos matemáticos y era
  considerado un trabajo repetitivo (nota de la traducción).

[^desertando-turing]: En la versión en inglés Turing usa un pronombre
  masculino, sin embargo el trabajo de computación era un trabajo
  feminizado, ver _The Computer Boys Take Over_ (nota de la traducción).

Así como las tecnologías que ahorran tiempo solo nos dieron más
ocupaciones, pasarles el trabajo de procesar números a las computadoras
no nos ha liberado del trabajo, en cambio ha integrado la computación en
cada faceta de nuestras vidas.  En la Rusia post-soviética, los números
te procesan[^desertando-post-soviet].

[^desertando-post-soviet]: Referencia al meme "en la Rusia soviética..."
  conocido como _Russian reversal_ \[inversión rusa\] (nota de la
  traducción).

Desde el principio, el objetivo del desarrollo digital ha sido la
convergencia del potencial humano y el control algorítmico.  Hay lugares
donde este proyecto ha sido completado.  La _pantalla Retina_ de los
iPhones es tan densa que el ojo humano al desnudo no puede determinar
que en realidad está compuesta por píxeles.  Aún existen grietas entre
las pantallas, pero se reducen día a día.

La red que cierra el espacio entre nosotres también cierra el espacio
dentro nuestro.  [Cerca los
comunes](https://crimethinc.com/blog/2013/06/10/prism-the-internet-as-new-enclosure/)
que han resistido la acumulación originaria.  Comunes como las _redes
sociales_ que solo podemos reconocer como tales ahora que han sido
mapeadas para su cercamiento.  Mientras la red crece para abarcar
nuestras vidas completas, debemos empequeñecernos para caber en sus
ecuaciones.  Inmersión total.

![Información en todos lados, comunicación en ninguno](https://cloudfront.crimethinc.com/assets/features/digital-utopia/computer-room.png)

Las grietas digitales
---------------------

A los liberales bien intencionados les preocupa que haya comunidades
enteras que aún no han sido integradas a la red digital global. De ahí
salen las computadoras gratuitas para los países "en vías de desarrollo"
y las _tablets_ de cien dólares para niñes en educación primaria.
Solamente pueden concebir el _uno_ del acceso digital o el _cero_ de la
exclusión digital.  Dado este binarismo, es preferible el acceso digital
--pero el binarismo en sí es un producto del proceso que produce
exclusión, no su solución.

> "Alguna vez nos dijeron que el avión había "abolido fronteras"; en
> realidad, desde que el avión se convirtió en un arma en serio las
> fronteras se volvieron definitivamente infranqueables" --George
> Orwell, ["La bomba atómica
> y vos"](https://web.archive.org/web/20130310044800/http://tmh.floonet.net/articles/abombs.html)

El proyecto de computarizar a las masas recapitula y extiende la
unificación de la humanidad bajo el capitalismo.  Nunca un proyecto de
integración se extendió tanto ni penetró tan profundo como el
capitalismo y pronto lo digital va a llenar por completo ese espacio.
"¡Los pobres aún no tienen nuestros productos!" --es el llamado a la
acción de Henry Ford.  _Amazon_ también vende _tablets_ por debajo del
costo, reconociéndolo como una inversión.  Les trabajadores individuales
se devalúan sin acceso digital; pero estar disponibles a un solo click,
obligades a competir inter-continentalmente en tiempo real, no hará que
se recupere el valor total de mercado de la clase trabajadora.  La
globalización capitalista ya lo ha demostrado.  Más movilidad para
individues no asegura mayor igualdad en general.

_Integrar_ no es necesariamente _igualar_: la correa, la rienda y el
látigo también son conectivos.  Aun cuando conecta, lo digital divide.

Al igual que el capitalismo, lo digital divide entre quienes tienen
y quienes carecen.  Pero una computadora no es lo que nos falta
a quienes carecemos.  Carecemos de _poder_, que no es repartido
ecuánimemente por la digitalización.  En lugar de un binario entre
capitalistas y proletaries, está surgiendo un mercado universal en el
que cada persona será evaluada y clasificada incesantemente.  La
tecnología digital puede imponer diferenciales de poder con mayor
profundidad y eficiencia que cualquier sistema de castas de la historia.

Nuestra habilidad para involucrarnos en relaciones sociales y económicas
de cualquier tipo está determinada por la calidad de nuestros
procesadores.  En el extremo más bajo del espectro económico, las
personas desempleadas que tienen un _smartphone_ consiguen el viaje más
barato en _Craigslist_ (donde hacer dedo era igualdad de oportunidades).
En el extremo más alto, el agente de bolsa de alta frecuencia y el
minero de Bitcoin lucran directamente del poder de procesamiento de sus
computadoras --haciendo parecer justa, en comparación, la compra-venta
de acciones pre-digital.

Es impensable que se pueda lograr la igualdad digital sobre un terreno
tan desnivelado.  La brecha entre ricos y pobrxs no fue saldada en los
países a la vanguardia de la digitalización.  Cuanto más amplio se
vuelve el acceso a lo digital, más experimentamos la aceleración de la
polarización  social y económica.  El capitalismo produce y circula
innovaciones con más rapidez que cualquier sistema anterior, pero
acarrea con ellas disparidades en aumento: allí donde los jinetes
dominaban a lxs peatones, ahora los bombarderos invisibles sobrevuelan
a les motoristes.  Podemos [imprimir armas en una impresora
3D](http://www.washingtonpost.com/blogs/the-switch/wp/2013/08/06/why-cody-wilson-the-3d-printed-gun-maker-thinks-3d-printing-might-not-take-off/),
pero la NSA puede [escribir gusanos
informáticos](http://spectrum.ieee.org/telecom/security/the-real-story-of-stuxnet/)
\[_computer worms_\] capaces de tomar el control de sistemas
industriales nacionales.  Y el problema no es solo que el capitalismo
sea una forma de competencia desleal, sino que esta forma de competencia
se impone en cada esfera de la vida.  La digitalización hace posible
incorporar su lógica hasta en los aspectos más íntimos de nuestras
relaciones.

La grieta digital no solo separa individues y demografías; nos
_atraviesa_ a todes.  En una era de precariedad, donde todes ocupamos
simultáneamente múltiples posiciones, cambiantes en lo sociales y en lo
económicas, las tecnologías digitales nos empoderan selectivamente de
acuerdo a las maneras en que somos privilegiades, mientras que ocultan
las formas en que somos marginalizades.  Le estudiante de posgrado que
debe cincuenta mil dólares se comunica con otres deudorxs por redes
sociales, pero es más probable que compartan sus currículums
o califiquen restoranes a que organicen un paro de deudorxs.

Solo cuando comprendamos a les protagonistas de nuestra sociedad en
tanto redes y no individues aislades, podremos chocarnos con la gravedad
de este hecho: la colectividad digital tiene por fundamento el éxito del
mercado, mientras que todes experimentamos el fracaso en aislamiento.
En las redes sociales del futuro --las que publicistas, agencias de
crédito, empleadores, arrendadores y policías vigilarán en una matriz
única de control-- puede que solo nos encontremos entre nosotres en la
medida en que afirmemos el mercado y nuestro valor en él.

El sistema se actualiza
-----------------------

La competencia y la expansión del mercado siempre han estabilizado al
capitalismo al ofrecer una nueva movilidad social, dándoles a lxs pobres
una razón para participar del juego justo cuando ya no les quedaba
ninguna.  Pero ahora que todo el mundo está integrado en un solo mercado
y que el capital se está concentrando en manos de una élite cada vez más
pequeña, ¿qué podría evitar una nueva ola de revueltas?

El antes mencionado Henry Ford fue uno de los innovadores que respondió
a la última gran crisis que amenazó al capitalismo.  Subiendo los
salarios y aumentando la producción en masa y los créditos, expandió el
mercado para sus productos --saboteando los reclamos revolucionarios del
movimiento obrero y transformando productores en consumidores.  Esto
fomentó que aun les trabajadores más precarizades aspirasen a la
inclusión en lugar de la revolución.

Las luchas de la generación siguiente se dieron en un terreno nuevo, con
consumidores que recuperaban el reclamo de les
productores[^desertando-productores] por su auto-determinación en el
mercado: primero como reclamo por la individualidad y luego, cuando esa
concesión fue lograda, por la autonomía.  Esto culminó con el clásico
imperativo de la contra-cultura hazlo-tú-mismx --"Convertirse en los
medios"-- justo cuando la infraestructura de las telecomunicaciones
globales fue miniaturizada para volver a les trabajdores individuales
tan flexibles como las economías nacionales.

[^desertando-productores]: Se utiliza productores en el sentido de
  trabajadores, no de capitalistas (nota de la traducción).

Nos hemos convertido en los medios y nuestro reclamo por la autonomía
nos ha sido dada --pero esto no nos ha vuelto libres.  Al igual que la
lucha de les productores se neutralizó convirtiéndoles en consumidores,
las demandas de les consumidores fueron neutralizadas volviéndoles
productores: donde los medios antiguos habían sido verticales
y unidireccionales, los nuevos medios derivan su valor del contenido
aportado por les usuaries.  Mientras tanto, la globalización y la
automatización erosionaron el compromiso que Ford había establecido
entre capitalistas y una franja privilegiada de la clase trabajadora,
produciendo una población redundante y precaria.

En este contexto volátil, nuevas corporaciones como _Google_ están
reversionando el compromiso fordista mediante el trabajo y la
distribución gratuitas.  Ford les ofreció a les trabajadores mayor
participación en el capitalismo mediante el consumo masivo; _Google_
regala todo al convertirlo en trabajo no pago.  Al ofrecer créditos,
Ford permitía a les trabajadores convertirse en consumidores al vender
su mano de obra futura junto con la presente; _Google_ ha disuelto la
distinción entre producción, consumo y vigilancia, haciendo posible
sacar provecho de aquelles que quizá jamás tengan algo para gastar.

La atención misma está supliendo al capital financiero como la moneda
determinante en nuestra sociedad.  Es un nuevo premio consuelo por el
que les precarizades pueden competir --aquélles que nunca serán
millonaries aún pueden soñar con un millón de vistas en _Youtube_-- y es
un nuevo incentivo para la constante innovación que el capitalismo
requiere.  Al igual que en el mercado financiero, tanto corporaciones
como individues pueden probar su suerte, pero quienes controlan las
estructuras a través de las que circula la atención empuñan el poder más
grande de todos.  La supremacía de _Google_ no deriva de las ganancias
publicitarias ni de ventas de productos sino de las formas en que modula
los flujos de información.

Si seguimos este camino, podemos imaginar un feudalismo digital en el
que tanto el capital financiero como la atención han sido consolidadas
en manos de una élite y una dictadura benevolente de computadoras
(quizás humanas) mantiene a la Internet como un parque infantil para una
población superflua.  Los programas y programadorxs individuales serán
reemplazables --cuanto mayor es la movilidad interna de una estructura
jerárquica, más robusta y resistente se vuelve-- pero la estructura
misma no será negociable.  Incluso podemos imaginarnos que el resto de
la población participará de manera aparentemente horizontal y voluntaria
en el refinamiento de la programación --dentro de ciertos parámetros,
claro está, como todo algoritmo.

El feudalismo digital podría arribar bajo la bandera de la democracia
directa, proclamando que cada quien tiene derecho a la ciudadanía y a la
participación, presentándose como una solución frente a los excesos del
capitalismo.  Quienes sueñan con un ingreso básico universal, o quienes
deseen ser compensades por la cosecha de sus "datos personales", deben
entender que estos reclamos solo podrían ser llevados a cabo por un
Estado de vigilancia todopoderoso --y que tales reclamos legitiman el
poder del Estado y la vigilancia aunque nunca sean concedidos.  Los
estadistas usarán la retórica de la ciudadanía digital para justificar
mapearnos a todes en nuevas cartografías del control, fijando a cada une
de nosotres a una sola identidad en línea con tal de cumplir su visión
de una sociedad sujeta a la regulación y cumplimiento totales.  Las
"ciudades inteligentes" impondrán el orden algorítmico al mundo
análogico, reemplazando el inmantenible imperativo de crecimiento del
capitalismo contemporáneo con nuevos imperativos: vigilancia,
resistencia y administración.  Las ciudades inteligentes no se basarán
en edificios más verdes, sino en la vigilancia y el control de nuestras
posesiones personales: _Walmart_ y otras corporaciones ya están
utilizando _chips_ RFID[^desertando-rfid], los mismos de los pasaportes,
para rastrear el flujo de sus bienes a través del mundo.

[^desertando-rfid]: Los chips RFID transmiten datos pasivamente
  y habilitan el control del flujo de cosas y seres (nota de la
  traducción).

En esta proyección distópica, el proyecto digital de reducir el mundo
a su representación converge con el programa de la democracia electoral,
en la que solo los representantes, mediante canales pre-establecidos,
pueden ejercer el poder.  Ambos proyectos se posicionan en contra de
todo lo incomputable e irreducible.  Fusionada como democracia
electrónica, nos presentarían la oportunidad de votar sobre una gran
variedad de asuntos minuciosos, mientras que vuelven incuestionable la
infraestructura que la habilita --_cuanto más participativo un sistema,
más "legítimo" se vuelve_.  Y sin embargo cada noción de ciudadanía
implica una parte excluida; cada noción de legitimidad política implica
una zona de ilegitimidad.

La libertad genuina es poder determinar nuestras vidas y relaciones
desde sus bases.  Debemos ser capaces de definir nuestro propios marcos
conceptuales, de formular las preguntas así como las respuestas.  Esto
no equivale a obtener mejor representación o más participación en el
orden reinante.  Abogar por la inclusión digital y la administración
estatal "democrática" da a quienes tienen poder las herramientas para
legitimar las estructuras a través de las que lo ejercen.

Es un error creer que las herramientas construidas para dominarnos nos
servirían si tan solo pudiésemos deshacernos de nuestros amos.  Es el
mismo error que cada revolución previa ha cometido respecto de la
policía, los tribunales y las prisiones.  Las herramientas de la
liberación deben ser forjadas en la lucha por alcanzarla.

![El logo de Google con la leyenda "feudalismo digital".](https://cloudfront.crimethinc.com/assets/features/digital-utopia/google.jpg)

Las redes sociales
------------------

Contemplamos un futuro en que los sistemas digitales atenderán todas
nuestras necesidades, siempre y cuando pidamos que el orden presente sea
con _envío instantáneo_.  Si trazamos la trayectoria de nuestro
imaginario digital, siempre estaremos votando, siempre trabajando,
siempre de compras, siempre encarcelades.  Incluso las fantasías que
separan el alma del cuerpo para viajar por dentro de la computadora
dejan intacto el sujeto liberal: absolutamente todos los post-humanismos
que nos han sido ofrecidos han sido neo-liberales.

Los gradualistas liberales que luchan por la privacidad en línea y la
neutralidad de la red configuran les subalternes a quienes defienden
como _individuos_.  Pero mientras sigamos operando de acuerdo al
paradigma de los "derechos humanos", nuestros intentos por organizarnos
contra los sistemas de control digital solo reproducirán sus lógicas.
El régimen de constituciones y estatutos que ahora se está acabando no
solo protegía al sujeto liberal, el individuo --también lo
inventaba.  Cada derecho del sujeto liberal implica un entramado de
violencia institucional orientado a asegurar su atomización
funcional --la repartición de la propiedad privada, la privacidad del
cuarto oscuro y las celdas de prisión.

Si hay algo que resalta la divulgación ostentosa de la vida cotidiana es
la fragilidad de la individualidad liberal.  ¿Dónde comienza y termina
el "yo", cuando mis conocimientos son derivados de motores de búsqueda
y mis pensamientos son desencadenados y dirigidos por posteos online?
Para contrarrestar esto, se nos incita a fortalecer nuestro frágil
individualismo mediante la construcción y diseminación de propaganda
autobiográfica.  El perfil en línea es una forma reaccionaria que
intenta preservar el último vestigio de subjetividad liberal mediante su
venta.  Llamémosle la "economía de la identidad".

Pero el objeto de explotación es una red, así como lo es le sujete en
revuelta.  Ninguno ha podido sostener una semejanza con al individuo
liberal.  Tanto el barco como la revuelta de esclavos son redes
compuestas de algunos aspectos de muchas personas.  Lo que las
diferencia no es el tipo de personas, sino los distintos principios de
interconexión.  Cada cuerpx posee múltiples corazones.  La perspectiva
que la representación digital provee para alumbrar nuestra propia
actividad nos permite clarificar que lo que perseguimos es un conflicto
entre principios organizacionales contrapuestos y no entre redes
o individues específiques.

Las redes que el liberalismo produce y oculta son inevitablemente
jerárquicas.  El liberalismo busca estabilizar la pirámide de la
desigualdad mediante una ampliación de su base.  Nuestro deseo es
aplanar las pirámides, abolir las indignidades de la dominación y la
sumisión.  No pedimos que los ricos den a les pobres; buscamos derrumbar
las cercas.  No podemos decir que lo digital sea _esencialmente_
jerárquico, porque no sabemos nada de "esencias"; solo sabemos que lo
digital es _fundamentalmente_ jerárquico, en el sentido de que se
construye sobre los fundamentos del liberalismo.  Si un mundo digital
diferente es posible, solo podrá surgir sobre una base distinta.

No necesitamos mejores versiones de tecnologías existentes; necesitamos
una mejor premisa para nuestras relaciones.  Las nuevas tecnologías son
inútiles salvo en la medida en que nos ayudan a establecer y defender
relaciones nuevas.

Las redes sociales son anteriores a la Internet; diferentes prácticas
sociales nos interconectan de acuerdo a diferentes lógicas.  Si
entendemos nuestras relaciones en términos de circulación en lugar de
identidades estáticas --o de trayectorias en vez de lugares, o fuerzas
en lugar de objetos-- podemos dejar de lado la cuestión de los derechos
individuales y emprender la creación de nuevas colectividades por fuera
de la lógica que produjo lo digital y sus grietas.

![Una asamblea alumbrada por
bengalas](https://cloudfront.crimethinc.com/assets/features/digital-utopia/fire1370.jpg)


La fuerza renuncia
------------------

Para cada acción, existe una reacción igual y opuesta.  La integración
forma nuevas exclusiones, les atomizades se buscan entre sí.  Cada nueva
forma de control crea un nuevo escenario para la rebelión.  La
vigilancia y la infraestructura de control han aumentado
exponencialmente en las últimas dos décadas, pero esto no ha creado un
mundo más pacificado --por el contrario, cuanta más coerción, más
inestabilidad y malestar.  El proyecto de controlar poblaciones mediante
la digitalización de sus interacciones y entornos es en sí misma una
estrategia para demorar las revueltas que seguirán a la polarización
económica, degradación social y devastación ecológica causadas por el
capitalismo.

La ola de revueltas que sacude el mundo desde 2010 --desde Túnez
y Egipto, pasando por España y Grecia, hasta el movimiento mundial de
_Occupy_ y más recientemente en Turquía y Brasil-- ha sido ampliamente
entendida como producto de las nuevas redes digitales.  Sin embargo
también es una reacción contra la digitalización y las disparidades que
refuerza.  Aunque las noticias de las ocupaciones circularon por
Internet, quienes las poblaron estaban allí porque estaban insatisfeches
con lo meramente virtual --o porque, siendo pobres o sin techo, no
tenían acceso para nada.  Antes de 2011, ¿quién hubiera imaginado que la
Internet produciría un movimiento mundial basado en la presencia
permanente en un espacio físico común?

Esto es solamente un anticipo de las resistencias que se opondrán
a medida que más y más partes de la vida se vayan encastrando a la
grilla digital.  Los resultados no están escritos, pero podemos estar
segures de que habrá nuevas oportunidades para que la gente se reúna por
fuerta de y contra la lógica del capitalismo y del control estatal.
Mientras que presenciamos la aparición de la ciudadanía digital y del
mercado de identidades, comencemos por preguntarnos qué tecnologías les
no-ciudadanes, les digitalmente excluides, necesitarán.  Las
herramientas empleadas durante la lucha por el parque Gezi en Istanbul
en el verano de 2013 podrían ofrecer un humilde punto de arranque.
¿Cómo podemos extrapolar del mapeo de protestas las herramientas que
serán necesarias para la insurrección y la supervivencia, especialmente
en cuanto las dos convergen?  Mirando a Egipto, podemos observar la
necesidad de herramientas que puedan coordinar la distribución de
alimentos --o deshabilitar el ejército.

Entender la expansión de lo digital como una acumulación originaria de
nuestros potenciales no significa dejar de usar las tecnologías
digitales.  Por el contrario, se trata de cambiar la lógica de abordaje.
Cualquier mirada positiva de un futuro digital será apropiada para
perpetuar y encubrir el orden dominante; la razón para participar del
terreno de lo digital es para desestabilizar las disparidades que
impone.  En lugar de establecer proyectos digitales en función de
prefigurar el mundo que queremos ver, podemos perseguir prácticas
digitales que interrumpan el control.  En lugar de buscar defender los
derechos de una nueva clase digital --o de incorporar a todes a esta
clase por medio de la ciudadanía universal-- podemos seguir el ejemplo
de les desposeídes, comenzando por las revueltas contemporáneas que
redistribuyen radicalmente el poder.

Entendides como una clase social, les programadores ocupan la misma
posición hoy que la burguesía en 1848, esgrimiendo un poder social
y económico desproporcionado respecto de su influencia política.  En las
revoluciones de 1848, la burguesía sentenció a la humanidad a dos siglos
más de infortunios al terminar aliándose con la ley en contra de les
trabajadores pobres.  Les programadores fascinades con la revolución de
Internet podrían hoy hacer algo incluso peor: podrían volverse les
bolcheviques digitales cuyo intento por crear una utopía democrática
termine por producir el más completo totalitarismo.

Por otro lado, si una masa crítica de programadores cambia su alianza
hacia las verdaderas luchas de les excluides, el futuro volverá a estar
en disputa.  Pero eso implica abolir lo digital tal y como lo conocemos
--y con ello, a les programadores como clase.  Desertemos de la utopía
digital.
