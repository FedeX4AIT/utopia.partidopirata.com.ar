---
author: Lili_Anaz y Natasha Akhmatova
title: Manifiesto por Algoritmias Hackfeministas
layout: post
signature: 0
toc: false
cover: assets/covers/single/manifiesto_por_algoritmias_hackfeministas_bn.png
contra: assets/images/Manifiesto-algoritmias-hackfeministas_03.png
---

# Manifiesto por Algoritmias Hackfeministas

**Lili_Anaz y Natasha Akhmatova**

Publicado en [Gender
IT](https://www.genderit.org/es/articles/edicion-especial-manifiesto-por-algoritmias-hackfeministas)

Nuestros cuerpos son territorios en disputa. Internet es un territorio
en disputa. Los algoritmos son un territorio en disputa. Hay una
tendencia de ánimos imperialistas que busca limitar las posibilidades de
nuestros cuerpos, sus afectos, relaciones y tránsitos para el servicio
mercantil de quienes deciden cómo se narra la historia y el mundo.

En este manifiesto --y más que intentar explicar qué es o qué no es un
algoritmo-- deseamos reescribir nuestras propias formas de intervención
y resistencia desde una postura política hackfeminista: queremos
resistir frente a toda infraestructura que permite y reproduce opresión,
discriminación y misoginia, a través de nuestros
cuerpos-territorios-algoritmos en cualquier espacio que habitemos dentro
o fuera de Internet.

Los algoritmos no deberían ser complacientes ni arrojarnos a lo
identificable o reconocible. Sus posibilidades de significar y
reimaginar mundos han sido reducidas a conceptos específicos que no nos
satisfacen ni mucho menos nos representan. Toda esa palabrería
impostada: relevancia, eficiencia, patrones, medición,
personalización... (Qué cansado, ¿verdad?) Pues la queremos fuera.

Al "algoritmo" le han impuesto un caracter casi místico: _sabemos_ que
existe y que nuestros datos están gobernados por el, pero no lo
conocemos: nunca lo hemos visto. Tampoco sabemos cómo funciona, cómo se
relaciona ni quiénes y por qué lo diseñan, ¿a quiénes responde? No
importa. Nada en ese _mundo_ está hecho para que nuestros cuerpos elijan
sobre sus territorios o sobre sus datos: es un _secreto de negocios_.

Este entendido de algoritmos es un tratado de _cajas negras_.
Desarrolladas por empresas privativas junto a los los principales
servicios comerciales _online_ --decidimos no mencionarlas porque
estamos hartas de leerlas por todas partes--, ¿qué los caracteriza? Su
naturelaza cerrada, extractivista y propietaria. ¿Y qué significa ésto?
Que es imposible acceder. Que no podemos analizar los factores que
determinan cómo priorizan y recomiendan. Que no entendemos qué criterios
consideran para la creación de "patrones de consumo" y "perfiles de
usuarios". Y lo más importante: que son precisamente ellos quienes nos
despojan de todo rastro de alteridad, convirtiéndonos a nosotras, a
nuestros cuerpos y a nuestros afectos, en su mina abierta de datos.

Rechazamos que _algoritmo_ sea sinónimo de aceptar la opacidad y la
_mística_ tecnológica propagandeada por corporaciones, la cual dicta que
los datos son de código cerrado, perfectos, sin errores, dotados de
absoluta verdad. Rechazamos la condescendencia automática: esa que
limita nuestro pensamiento e imaginación sobre otras formas de acuerpar
las resistencias por las que sí queremos luchar. Así que lo tenemos
claro: estamos negadxs a que nos arrebaten la imaginación para actuar.

Queremos que los algoritmos sigan siendo matemáticos pero nunca más
dogmáticos: vamos a extirparles todo cálculo colonialista. Todo aspecto
prescriptivo. Toda ambición unificante. Vamos a castrarle de una vez por
todas su asquerosa hambre misógina.

Queremos defender la curiosidad y la exploración frente al miedo. Frente
a las amenazas de violencia. Frente a la censura y al empobrecimiento de
interconexiones posibles. Frente a lo que se nos muestra como lo
relevante por encima de lo que quizás desearíamos experimentar.
**Queremos algoritmias hackfeministas** para romper los discursos
dominantes que reproducen al sistema patriarcal a través de las
tecnologías.

**Queremos resistir.**

![](/assets/images/Manifiesto-algoritmias-hackfeministas_02.png)

### Con cuerpos que defienden sus ritmos

Que los algoritmos, como cualquier operación matemática, reconozcan los
movimientos de intento, prueba y error. Que los canales de entrada y
salida, así como cada uno de sus procedimientos y variables de estado o
tiempo, estén en clave de código abierto.

Que la repetición de imágenes e informaciones en nuestros _feeds_ se
convierta en ritmos expresivos: no más consumo impositivo, sometimiento
y _force feeding_. Queremos meter a nuestros cuerpos sólo lo que nos da
placer.

Que pasado y presente no cedan el control de nuestra memoria a la
promesa de futuros empaquetados en forma de predicciones,
recomendaciones y propaganda.

Nunca más mandatos de comportamiento sobre cómo presentarse, reaccionar,
responder y narrar. Queremos cuerpos que disfruten, no que se carcoman
de ansiedad.

**La algoritmia hackfeminista es el pulso que programamos nosotras para
sintonizar nuestros beats.**

### Con cuerpos mutantes hacia existencias gozosas

Los términos y condiciones los ponemos nosotrxs. No queremos tener que
aceptar nada.

Nuestros nombres los eligimos nosotrxs. Nuestras muchas representaciones
las elegimos nosotrxs.

_To agree or not to agree_: nuestro consentimiento no es binario.

Subvertimos nuestras propias versiones y construcciones de identidades,
paisajes, gestos y futuros.

**La algoritmia hackfeminista comprende cómo funciona esa versión de
mundo y desde ahí programa variables críticas para intervenirlo.**

### Con cuerpos que habitan, sostienen, desarrollan y combaten

No dejamos lugar a que tecnología sea sinónimo exclusivo de máquinas,
dispositivos y plataformas de redes sociales corporativas.

Ampliamos nuestros imaginarios: construimos estrategias de cuidado, gozo
y combate como tecnología.

Combatimos todo protocolo normativo desde los cuerpos preparados para
pelear: conocemos nuestra fuerza, sostenemos nuestras prácticas y
estudiamos profundamente nuestros campos de batalla.

Somos la infraestructura que nos sostiene. Desarrollamos redes,
servidoras, antenas, archivos y memorias.

**La algoritmia hackfeminista acompaña nuestras autodefensas: es la
fuerza de nuestros cuerpos juntos y la amplificación de su potencia.**

### Con cuerpos como bombas de cultivo frente a las narrativas automáticas

Las máquinas de guerra convierten a la guerra en paisaje: la presentan
amigable, limpia, complaciente, necesaria y en alta definición.

Bajo interfaces simples, planas y funcionales que pretenden
invisibilizarnos, polinizamos paisajes desde los bordes, las fallas, los
errores y los **glitches**.

Cultivamos ecosistemas de deseo y nos contagiamos de formas caóticas
entre nosotras: buscamos provocaciones comunes y acciones distribuidas,
imprevisibles e interconectadas.

Estamos armadas para distinguir el código de programación del loop
infinito (i = misóginocolonialracistaysexista). Vamos a intervenir en su
sentencia que está programada para ser siempre verdadera. La romperemos
en pedazos comprobando su falsedad hasta bloquearla con fuerza.

**La algoritmia hackfeminista es el comando corporal que frena este
jodido loop.**

_[Este Manifiesto se escribió desde la complicidad algorítmica a cuatro
manos y en portuñol-espanglish entre México y Brasil de mayo a octubre
de 2017, principalmente a través de un larguísimo pad <3, chats, notas
de voz, spokenword, notas en video, imágenes, desvelos, debrayes, mails,
pausas, llamadas, música y sobre todo, ganas. En una segunda etapa,
desde el verano mexicano y el invierno brasileño del 2018, buscamos
crear un co-relato visual en diálogo con las palabras para darle vida a
la Algoritmia a través de una narrativa que también muta, se amplía y
decodifica]._

![](/assets/images/Manifiesto-algoritmias-hackfeministas_code.png)

## Glosario

**Algoritmo:** En matemáticas, lógica y computación, es un conjunto
prescrito de instrucciones o reglas bien definidas, ordenadas y finitas,
que permite llevar a cabo una actividad mediante pasos sucesivos que no
generen dudas a quien deba hacer dicha actividad. En programación, es
una secuencia de pasos lógicos que permiten solucionar un problema.

**Loop:** En computación, una secuencia de instrucciones que se repite,
ya sea un número específico de veces, o hasta que se cumple una
condición en particular. En electricidad, un circuito cerrado.

**Beat:** Significa "latido" en inglés. En música, una sucesión
constante de unidades de ritmo.

**Force feeding:** Derivación de la expresión del inglés ‘force-feed’:
obligar a una persona o animal a comer y beber, a menudo poniendo comida
en el estómago a través de una pipa en la boca.

**Glitch:** Expresión en inglés proveniente de la electrónica: refiere a
un error en un archivo, y se visualiza comúnmente con la _pixelización_
de la pantalla a partir de una falla en un sistema electrónico.

## Co-autoras

**Lili_Anaz** (México) es comunicóloga, escritora, fotógrafa y artista
hackfeminista. Sus proyectos exploran la intersección entre arte,
cuerpos, redes, memoria, sexualidades, resistencias, tecnologías libres,
ciberfeminismos y cuidados colectivos digitales. Es autora del proyecto
artístico transmedial Mirada sostenida, co-fundadora y coordinadora del
Laboratorio de Interconectividades y cómplice del proyecto Autodefensas
Hackfeministas.

<https://twitter.com/lili_anaz>

<https://lab-interconectividades.net>

<https://miradasostenida.net>

<https://lab-interconectividades.net/autodefensas-hackfeministas>

**Natasha Akhmatova** (Brazil) creció en IRC y aprendió en la comunidad
Linux qué era hackear. Su trabajo explora la intersección entre
tecnología, activismo, artes y medios y plataformas de comunicación. Es
idealizadora y coautora de la guia Safer Nudes! y Chupadados.org, y
trabaja con la formulación de programas para difusión científica con una
perspectiva interseccional.

<https://twitter.com/natashafelizi>

<https://www.codingrights.org/safernudes/>

<https://www.codingrights.org/3-2/>

## Collage-ilustraciones

**Ana Cristina Joaquim** (Brazil) es poeta, investigadora en Unicamp,
Fapesp y profesora universitaria. También se arriesga en trabajos
visuales con cierta frecuencia.

<https://1n.pm/buscatextual>

### Edición digital de ilustraciones

**Melissa Aguilar** (México-Costa Rica) es diseñadora y artista visual.
Se dedica a la investigación en nuevos medios, museos, tecnologías
inmersivas y abiertas. Colabora con el fablab Inventoria en CR y con
Medialabmx.

<https://www.behance.net/meliaguilar>

## Licencias libres <3

Entendemos las licencias como formas de explicitar **pactos comunes**
que nos ayuden a nombrar, conversar, reescribir y \[co\]crear. Estas
licencias o pactos conversan y construyen aportando a los movimientos de
cultura y licencias libres que han ido emergiendo en los últimos años.

La [Licencia Producción de Pares: Atribución - Compartir bajo la misma
licencia - No capitalista](https://endefensadelsl.org/ppl_deed_es.html).

La [Licencia Creative Commons: Atribución - No comercial - Compartir
igual](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es)
