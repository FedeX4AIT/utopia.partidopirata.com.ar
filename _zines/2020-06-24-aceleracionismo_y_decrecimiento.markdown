---
layout: post
title: "Aceleracionismo y decrecimiento"
author: "Aaron Vansintjan"
---

## La extraña pareja de la izquierda

Durante este último tiempo, he estado involucrado con un grupo en
Barcelona que estudia y aboga por el "decrecimiento": la idea de que
debemos reducir producción y consumo para lograr una sociedad más
equitativa, y para lo cual debemos desmantelar la ideología del
"crecimiento económico a toda costa". Como podés imaginarte invierten
mucho de su tiempo intentando  aclarar malentendidos: "No, no estamos
contra el crecimiento de los árboles. Sí, también deseamos que les niñes
crezcan. Sí, también nos gustan las cosas agradables como la asistencia
sanitaria".

Pero este último año  estuve viviendo en Londres. Allá, la ideología
activista pareciera estar influenciada por les "aceleracionistas",
quienes sostienen que el capitalismo y sus tecnologías deberían
impulsarse más allá de sus propios límites para crear un nuevo futuro
post-capitalista. El aceleracionismo es como si, habiendo intentado por
todos los medios evadirse de un agujero negro, la tripulación de la nave
decidiera que lo mejor sería pegar la vuelta y dejarse chupar: "¡Ei,
podría haber algo copado del otro lado!"

Luego de un año de experiencias en algunos círculos activistas de
Londres, ahora entiendo mejor de donde viene esto. Décadas de recortes
gubernamentales, aplastamiento de los sindicatos, financialización total
de la city, y la falta de recursos para la organización comunitaria
pusieron al activismo londinense en modo crisis: exhausto, aislado
y siempre a la defensiva.

Estos mundos de pensamiento estan bien resumidos en dos libros
recientes. En *Decrecimiento: Un vocabulario para una nueva era*,
editado por Giacomo D'Alisa, Federico Demaria y Giorgos Kallis, sus
autores explican conceptos como "cuidado", "justicia medio ambiental",
"renta básica", "bienes comunes", todos los cuales son vistos como parte
del encuadre interpretativo del decrecimiento. Para ellos, decrecimiento
es un término abarcativo que alberga una variedad de movimientos,
ideologías e ideas para un mundo más sustentable y menos capitalista.

En *Inventando el Futuro: Postcapitalismo y un Mundo Sin Trabajo*, sus
autores Alex Williams y Nick Srnicek discuten la promesa de una renta
básica, el crecimiento de las tecnologías de automatización y del
pensamiento utópico para crear una especie de "comunismo de lujo
totalmente automatizado".

Sorprendentemente, ambos libros tienen mucho en común. Tenés los
imaginarios utópicos, un renovado foco en economías alternativas, la
voluntad de pensar más allá del neoliberalismo, tanto como del
keynesianismo y la habilidad de lidiar con los efectos contemporáneos de
la tecnología en la sociedad y el medio ambiente.

Pero al mismo tiempo tienen bastantes diferencias. Estas diferencias se
me hicieron evidentes el invierno pasado, una tarde gris de sábado
durante un evento llamado "Future Society Forum"[^ayd-1]. Después de una
corta introducción a cargo de Nick Snricek, activistas de los
alrededores de Londres eran invitades a un _brainstorm_[^ayd-2] acerca de
cómo podría verse una utopía de izquierdas.

La sala fue dividida por diferentes "temas": trabajo, salud, medio
ambiente y recursos, educación, etc. Se nos propuso que primero
colocáramos post-its[^ayd-3] con ideas sobre "futuros" particulares
a cada tema. Cómicamente, alguien había puesto "renta básica" en cada
uno de los temas, incluso antes de que el evento hubiera comenzado (¿un
intento de mensaje subliminal?). Luego, se nos pidió que nos
dividiéramos en grupos para discutir cada tema.

Dada mi formación, decidí que donde mejor podía contribuir era en el
tema de medio ambiente, aunque estaba ciertamente interesado en sumarme
a los otros.  Después de una discusión de 15 minutos, llegó el momento
de que cada grupo hiciera una devolución al colectivo. Como era de
esperar, el grupo de medio ambiente imaginó una sociedad descentralizada
donde los recursos serían gestionados por bioregiones; una economía de
baja tecnología, bajo consumo, donde todes tienen que hacer un poco de
horticultura y un poco de limpieza, y donde la ciudad está perfectamente
integrada al campo. Estoy casi seguro de haber escuchado risitas
mientras que nuestra utopía era leída en voz alta.

Por otro lado, el grupo "trabajo", avisoró un futuro con máquinas que
harían todo por nosotres, requiriendo grandes fábricas, en las que todas
las tareas, si las hubiera, serían remuneradas equitativamente, donde
nadie tendría que hacer lo que no guste, en donde sistemas
computarizados de alta tecnología controlarían la economía. Básicamente
el sueño del "lujo comunista totalmente automatizado".

Hablando de sesgo de selección.

Sin embargo, parte de mí esperaba algo más que unas risitas. Pero el
desafío directo nunca llegó. Les aceleracionistas concedieron a les
ambientalistas su utopía come-gusanitos mientras elles rumiaban sus
propios tecno-fetiches. ¿Era sólo un armisticio con la intención de
prepararse para una batalla más grande por venir o había realmente menos
animosidad de la que yo imaginaba?

Por supuesto que este tipo de diferencias no son totalmente nuevas para
la izquierda; líneas opuestas similares tuvieron lugar en otros
movimientos sociales del pasado: ¿debemos destruir las máquinas
o usarlas en nuestro beneficio? Quizás Friedrich Engels haya desestimado
totalmente a les campesines como posibles revolucionaries, pero el
anarquista ruso Mikhail Bakhunin insistía en que les campesines podían
y serían cruciales para crear un mundo más allá del capitalismo, y que
la izquierda podría aprender de las comunidades campesinas para darse
una idea del aspecto que tendría otro mundo.

Estas mismas tensiones son las que rivalizan en las ideologías
aceleracionistas y decrecentistas. Aceleracionistas como Srnicek
y Williams ponen el énfasis en la automatización, el rol de los
sindicatos y la reducción de la semana de trabajo como las variables
principales para cambiar el rumbo más allá del capitalismo. Su foco está
puesto en las cosas grandes (fuerza laboral, comercio global)
y sostienen que poner el foco en pequeñas intervenciones de la izquierda
es parte del problema y no de la solución. Las académicas del
decrecimiento apuntan a pequeñas _nowtopias_[^ayd-4] y a hacer alianzas
con aquelles que luchan contra el extractivismo: frecuentemente
campesines, pobladores rurales y pueblos originarios.

Cuando terminé de leer el libro de Srnicek y Williams, me di cuenta de
que decrecimiento y aceleracionismo (aunque supe que ahora Williams
y Srnicek se distancian del término para no ser confundidos con las
variables más derechistas del movimiento) realmente tienen más en común
de lo que originalmente creía: tanto en términos prácticos (políticas
y estrategias) como en sus posiciones ideológicas generales. Y tienen
mucho que aprender unes de otres.

Lo que sigue es una especie de reporte: una conversación entre las dos
propuestas.  Habrá algo de crítica, pero tambien algo de polinización
cruzada. Mi debate gira en torno a un par de temas: la importancia del
pensamiento utópico, la tecnología, la economía y la estrategia
política.

Si bien hay coincidencias, también hay diferencias. ¿Cómo es posible que
habiendo tantos consensos, tengan encuadres tan diferentes sobre el tema
tratado? A modo de síntesis, señalo que la noción de "velocidad" y sus
diferentes posturas al respecto, son fundamentales a ambas posiciones.


## Pensamiento Utópico

Como lo expresó David Graeber, en otro jugoso ensayo más, los
movimientos sociales de hoy están experimentando una especie de "fatiga
desesperada": no satisfechos con la simple queja  sobre los recortes
a los servicios sociales, hacen renacer un pensamiento positivo
y futurista.

De hecho, pareciera que un principio unificador clave entre
aceleracionismo y decrecimiento es su promoción de ideas utópicas. Esto
podría sorprender a aquelles que no están familiarizades  con la
literatura del decrecimiento; recientemente, se ha dedicado un libro
entero a atacar la hipótesis del decrecimiento acusándola de
anti-moderna y basada en una "ecología de la austeridad".

Como sea, el hecho es que les pensadores del decrecimiento han puesto un
montón de pensamiento sobre cómo superar una evasiva primitivista de la
modernidad, y concebir un futuro bajo en carbono, democrático
y justo. A pesar de las connotaciones negativas que puedan resultar de
una palabra como "decrecimiento", ha habido propuestas muy positivas
y progresistas dentro del movimiento. En este sentido, las claves
conceptuales incluyen el "deseo", es decir el énfasis sobre el hecho de
que una transición justa no debe ser forzada sino provenir de la propia
voluntad política de la población; "comunalidad de bienes", por la cual
los recursos son gestionados colectivamente en lugar de privatizados; el
respaldo de políticas innovadoras tales como renta básica mínima
y máxima, así como reformas impositivas ecológicas; la resucitación del
reclamo de Paul Lafarge por el "derecho a la pereza", también rescatada
por les aceleracionistas; la adopción de imaginarios inspirados por
_nowtopias_, existiendo actualmente experimentos de subsistencia
que apuntan a diferentes futuros posibles.

Lo mismo es cierto para les aceleracionistas. De hecho, Srnicek
y Williams remarcan la renuncia a lo imaginativo de la mayor parte del
activismo de izquierda durante las décadas pasadas. Para ellos, el
activismo progresista ha sido limitado en gran parte por lo que
denominan _folk politics_[^ayd-6], una ideología activista reducida en
su ámbito, que se enfoca en acciones inmediatas y temporarias, en vez de
organizarse para el largo plazo; se centra en el intento de crear
perfectos "micro-mundos" prefigurativos, en lugar de lograr cambios de
amplio espectro en el  sistema. Esto, arguyen, es sintomático de un
momento político más amplio, en el cual el consenso neoliberal ha
bloqueado la posibilidad de pensar políticas y mundos
alternativos. Y entonces proponen una visión del futuro que es al mismo
tiempo, moderno y conciente de los lineamientos económicos de la
actualidad. Al igual que el movimiento decrecentista, proponen que la
ideología dominante del pro-trabajo debe ser desmantelada, pero
a diferencia del decrecimiento, proponen un mundo donde la gente no
tenga que someterese al trabajo pesado sino que pueda perseguir sus
propios intereses dejando a las máquinas que hagan todo el trabajo; en
otras palabras, "comunismo de lujo totalmente automatizado".

Lo que une ambas visiones es una estrategia contra-hegémonica que
configura imaginarios y éticas alternativas, que desafían el momento
neoliberal insistiendo en que otros mundos son posibles y de hecho,
deseables. Para académicos del decrecimiento, como Demaria y otres, el
decrecimiento no es un concepto unívoco, sino un marco interpretativo
que reune una constelación de términos y movimientos.  Para les
aceleracionistas, parte de la estrategia es promover un nuevo conjunto
de demandas "universales" que permitan que un nuevo desafío político
tome lugar.  Además, convocan a una "ecología de organizaciones", grupos
de reflexión, organizaciones no gubernamentales, colectivos, grupos de
presión, sindicatos, que puedan entramar juntes una nueva
hegemonía. Para ambos, existe una necesidad de socavar las ideologías
existentes a través de, por un lado, oponer fuertes objeciones, y, por
el otro, establecer nuevas ideologías (p. ej. postrabajo,
convivialidad). El resultado es: dos fuertes propuestas para futuros
alternativos que no tienen miedo de soñar en grande.

## ¿Pluralismo económico, monismo político?

Cuarenta años más tarde de que Irving Kristol, el padrino del
neo-conservadurismo, en su conocido discurso en la Mont Pelerin Society,
acusara a la Nueva Izquierda de "negarse a pensar económicamente", es
interesante que estos dos esquemas emergentes vuelvan a centrar lo
económico en sus análisis. Efectivamente, ambos esquemas proponen
políticas económicas asombrosamente similares. Comparten demandas tales
como la renta universal, la reducción de las horas laborales, y la
democratización de la tecnología. Sin embargo, difieren en otros
reclamos: Williams y Srnicek destacan el potencial de la automatización
para enfrentar la desigualdad, y del centrarse en el rol de los avances
tecnológicos, bien  para  acrecentar la precareidad o, contrariamente,
para liberar la sociedad. En este sentido, hablan extensamente sobre la
importancia de la innovación y los subsidios para investigación
y desarrollo liderada por el estado y cómo esto debe ser reclamado por
la izquierda.

Por el contrario, académicos del decrecimiento como Giorgos Kallis
y Samuel Alexander han propuesto una plataforma de políticas más
variada, pasando por la renta mínima y máxima, la reducción del horario
laboral y tiempo compartido, reforma bancaria y financiera, planeamiento
y presupuestación participatorias, reforma impositiva ecológica, soporte
financiero y legal a la economía solidaria, reducción de la publicidad
y abolición del uso de PBI como un indicador de progreso.  Estas son
solo algunas de las políticas propuestas por les defensores del
decrecimiento; de todos modos, el punto es que les decrecentistas
tienden a respaldar una amplia plataforma de políticas más que un
conjunto de cambios estratégicos facilistas en el sistema.

En muchos puntos de su libro, Srnicek y Williams instan a la izquierda
a comprometerse nuevamente con la teoría económica. Argumentan que
mientras la economía dominante necesita ser desafiada, herramientas
tales como modelización, econometría y estadística serán cruciales en el
desarrollo de una visión del futuro revitalizada y positiva.

En efecto, casi al final de libro, hacen una apuesta por una economía
"pluralista".  En los inicios de la crisis del 2008, la izquierda
respondió con un "keynesianismo improvisado", porque, habiendo estado el
foco puesto durante mucho tiempo en la crítica al capitalismo, había una
severa falta de teorías económicas alternativas disponibles de las
cuales servirse. Ellos instan a pensar a través de problemas
contemporáneos que no son fácilmente abordables por la teoría económica
keynesiana o marxista: estancamiento secular, "el desplazamiento hacia
una economía informacional, post-escasez", abordajes alternativos a la
expansión cuantitativa, y las posibilidades de la automatización total
y de la renta básica universal, entre otros. Argumentan la necesidad de
que la izquierda "piense a través de un sistema económico alternativo"
que se base en tendencias innovadoras, incluyendo  "desde teoría
monetaria moderna hasta economía de complejidad, desde economía
ecológica a participatoria".

De todos modos, me sentí decepcionado por lo que ellos consideran formas
"plurales" de economía. Hubo poca discusión acerca del contenido de
economías alternativas tales como economía institucional, economía
poskeynesiana, teoría de los bienes comunes, economía del medio
ambiente, economía ecológica y teoría del posdesarrollo.  Son estos
campos los que han ofrecido algunos de los mayores desafíos a la
economía neoclásica y que presentan, al mismo tiempo, fuertes desafíos
a su propia ideología política. Harían bien en vincularse más con estas
teorías.

Esta laguna no es menor, más bien refleja problemas más profundos dentro
de todo el esquema aceleracionista. Para un libro que menciona el cambio
climático como uno de los principales problemas que enfrentamos, incluso
señalándolo en la primera oración de su Manifiesto Aceleracionista, se
implica bastante poco con el tema medioambiental. Y sin embargo, son
estos campos no tratados de la economía heterodoxa los que han provisto
algunas de las respuestas más útiles a la actual crisis medioambiental,
incluso yendo tan lejos como para proveer modelos robustos y análisis
econométrico para probar sus propios argumentos.

Esta misma carencia no se encuentra en la literatura del
decrecimiento. De hecho, el movimiento ha sido en gran parte inspirado
por economistas rebeldes como Eleanor Ostrom, Nicholas Georgescu-Røegen,
K. William Kapp, Karl Polanyi, Cornelius Castoriadis, Herman Daly and
J.K. Gibson-Graham. Las  sesiones sobre decrecimiento son ahora la norma
en muchas conferencias económicas, así como las conferencias sobre
decrecimiento están ampliamente dominadas por la discusión de la
economía.

Tomándose con calma las lecciones de la economía institucional, les
pensadores del decrecimiento han remarcado que no existe panacea: no
habrá una única política que realice el truco, es necesaria una
plataforma política diversa y complementaria para compensar los
circuitos de retroalimentación que podrían surgir de la interacción de
diferentes políticas.

Desde esta perspectiva, las políticas estratégicas propuestas por les
aceleracionistas (renta básica, automatización, reducción del horario
laboral) comienzan a verse más bien simplistas. Enfocarse en tres
políticas centrales funciona para una lectura elegante y para simples
pancartas, pero tiene un precio: cuando estas políticas sean
implementadas y resulten en efectos negativos no previstos, habrá escasa
voluntad política para continuar experimentando con ellas.  Yo apostaría
por una plataforma sólida de políticas múltiples, suficientemente
resiliente para lidiar con circuitos de retroalimentación negativa y sin
dogmatismos acerca de cuál debería ser implementada en primer término.

Un punto fuerte de les aceleracionistas es su énfasis en el aspecto
político de las medidas económicas, que por lo tanto deben ser
conseguidas mediante la organización política. De esta forma, dan un
paso crucial más allá del economicismo, término que Antonio Gramsci
utilizaba para referirse a les izquierdistas que ponían el activismo
contrahegemónico en segundo plano hasta que las "condiciones económicas"
lo favorecieran. No podría decirse lo mismo sobre los ecologistas de
izquierda: la escasez, los límites medioambientales son, frecuentemente,
impuestos como espectros apolíticos que invalidan toda otra
preocupación.

Pero más allá de todos sus llamados por una visión unificada y utópica,
permanezco aprensivo con respecto al tipo de utopía que proponen y, por
lo tanto, al tipo de políticas que consideran necesarias. Mientras que
_folk politics_ [^5] es en parte una promisoria definición del activismo
que no logra escalar, también puede convertirse en una manera de
desestimar cualquier cosa que no encaje en su idea de lo que la política
*realmente* es.

Tomemos como ejemplo la subestimación aceleracionista de la respuesta
popular de la Argentina a la crisis financiera. Bajo su mirada, el "giro
nacional de gran escala hacia el horizontalismo" que involucró asambleas
barriales después de la recesión de 1998, "se quedó en una respuesta
localizada a la crisis" y "nunca se acercó al punto de reemplazar al
estado". La dirección de las fábricas por les trabajadores no logró
tomar impulso y "permaneció necesariamente insertada dentro de las
relaciones sociales capitalistas". Como conclusión, afirman que el
"momento" argentino fue simplemente un alivio para los problemas del
capitalismo y no una alternativa al mismo".  Elles sostienen que fue
simplemente una respuesta de emergencia y no un competidor.

Pero este es un punto de vista realmente problemático de lo que
constituye "lo político". Basado en décadas de informes sobre luchas
populares latinoaméricanas y su participación en ellas, Raúl Zibechi
sostiene, luego del abandono neoliberal del estado, campesines, pueblos
originarios y habitantes de los barrios marginales están creando nuevos
mundos y recursos que operan  distinto a la lógica del estado y el
capital. Estas nuevas sociedades no hacen demandas a los partidos
políticos y no desarrollan agendas para reformas a través de
elecciones. En cambio, organizan "con/contra" instituciones existentes,
a través de "reterritorializar" sus medios de vida, construyendo
economías diversas y horizontales, y organizando levantamientos en
momentos coyunturales.

Según la mirada de Zibechi, la mismísima reacción popular argentina se
describe como un momento en el cual "lo inviable queda a la vista". Lo
que estaba latente bajo la superficie se revela "como un rayo que
ilumina la noche". Más que ser una "respuesta de emergencia", la
contestación argentina fue estratégica y puesta en práctica no tan
espontánea y desorganizadamente como Srnicek y Williams lo pintan.

De igual modo, con las políticas de género. Mismo si Williams y Srnicek
reconocen las teorías económicas feministas alrededor de los trabajos de
cuidado y reproductivos, lo que, para ellos, califica como política
"real" cae en reinos bien hegemónicos: lobby, la formación de _think
tanks_ [^6], política de plataformas, sindicatos y modelización
económica. Pero ¿qué hay acerca de otros tipos de resitencia, como los
que Zibechi resalta: colectivos de cuidado infantil, asentamientos
ocupados y autónomos, escuelas y clínicas organizadas comunalmente,
comedores populares y piquetes? ¿Cómo esas prácticas, ahora contempladas
como "comunalización", entran en su "ecología de  organizaciones"?

Me preocupa que la desestimación de les campesines como agentes
revolucionaries, que esgrimen aceleracionistas como Friedrich Engels,
implícitamente resulte en un rechazo a la posibilidad de potenciales
alianzas con las luchas de los pueblos originarios y anti
extractivistas. Si el éxito político es medido solo por sus logros
estatales, entonces las victorias no estatistas permanecerán invisibles.

Por el contrario, les pensadores decrecentistas han colaborado con
académicas del posdesarrollo como Ashish Kothari y Alberto Acosta y han
ayudado a crear una red de justicia medioambiental estableciendo
alianzas, precisamente con los grupos que serían los más afectados por
un aumento de la automatización y quienes probablemente se beneficiarían
menos con las políticas aceleracionistas como la renta básica.

Desafortunadamente lo que Srnicek y Williams llaman _folk politics_
termina justificando su visión específica de lo político, que
notablemente es una visión del norte, incapaz de romper con ideas
hegemónicas acerca de les actores políticos correctos. Con esta lógica,
el movimiento argentino "falló" porque no pudo replicar o reemplazar el
estado. Les sería muy útil relacionarse con teorías subalternas,
estudios sobre decolonialización, académicas del posdesarrollo, todes
quienes de diferentes maneras han desafiado las concepciones
occidentales acerca de cómo debería ser la resistencia, las alternativas
y el progreso. Más aún, deberían vincularse con teóricas de lo comunal,
quienes demuestran cómo las prácticas comunalistas abren alternativas
reales al neoliberalismo. Más allá de las alianzas teóricas, esto les
ayudaría a no catalogar los movimientos como "fallidos" simplemente
porque no buscan copiar al estado.

## Tecnología, Eficiencia y Metabolismo

Para mucha gente de izquierda la tecnología es secundaria a las
políticas redistributivas (bienestar, salud, empleo, equidad) y la
innovación es el reino de las compañías privadas, no del estado.

En contraposición, les aceleracionistas reconocen que la tecnología es
un factor clave del cambio social y económico. Para Srnicek y Williams,
un objetivo dentro de la izquierda sería politizar la tecnología para
transformar las máquinas capitalistas hacia fines socialistas. Si vamos
a lidiar con los múltiples problemas que enfrenta hoy la humanidad
debemos tomar el reinado de la tecnología, democratizarlo. Este ademán
"moderno", que evita el primitivismo y el deseo de retornar a un pasado
"más simple", es realmente apreciado.

Srnicek y Williams emplean gran parte del libro discutiendo la manera en
que la automatización está transformando las relaciones económicas
y sociales a nivel mundial. La robotización no solo está convirtiendo
a tantos trabajadores del norte global en inútiles, sino que la
automatización está comenzando a tener sus efectos en países que se
desarrollan rápido, como China. Llegan tan lejos como para relacionar la
informalización de una gran parte de la humanidad (residentes de barrios
pobres, migrantes rurales-urbanos) como una indicación de que el
capitalismo ya no necesita su "reserva de mano de obra". El
establecimiento de la automatización significa que entraremos una vez
más, en un mundo de desempleo masivo, en el cual el trabajo se vuelve
barato y todo el poder queda en las manos del empleador.

Su respuesta a esto es bastante audaz: antes que escapar a esta
"realidad" moderna, sugieren impulsar sin fin el crecimiento de la
automatización, acabando eventualmente con la necesidad de trabajo
repetitivo y dando lugar a un "comunismo de lujo completamente
automatizado"; su visión de un futuro deseable. Como parte de esto
argumentan que la inversión pública en innovación será clave para lograr
este objetivo.

Como tratan de demostrar, la automatización ya está ayudando a la
desindustrialización en varios países (desarrollados y en vías de
desarrollo), lo cual significa que sin importar si la completa
automatización se realiza o no, hay una necesidad crítica de que los
movimientos sociales luchen por avances políticos para garantizar redes
de protección social. En respuesta a esto sostienen que los sindicatos
deberían estar peleando por menos horas de trabajo, no más, y que la
renta básica ayudará a abordar el desempleo masivo que la automatización
parece estar causando.

Concuerdo con que este tipo de respuestas políticas serán necesarias en
los años por venir y que la automatización ciertamente plantea dilemas,
pero por varias razones que listaré más abajo no estoy seguro si
representa *el problema central*, como ellos parecen afirmar. Antes que
nada ¿Está realmente la automatización aconteciendo a un paso tan
acelerado y destructivo? Es cierto que la tasa de crecimiento del empleo
global está disminuyendo, pero esto puede ser explicado por un número de
factores, muchos de los cuales están siendo señalados cada vez más por
economistas convencionales: la aparición de un "estancamiento secular"
en Europa y América, el declive en la extracción petrolera convencional
y el agotamiento del cremiento "fácil" que ya se sentía en los años
70. De hecho, habiendo escarbado entre sus citas, no encontré mucha
investigación que muestre cómo se compara el rol de la automatización
con estos otros factores en las transformaciones económicas actuales. De
todas formas, no siendo un economista laboral, no estoy lo
suficientemente versado en los números para continuar la discución. En
esta ocasión, les daré el beneficio de la duda.

Segundo, y más problemáticamente, sigo a George Caffentzis en su
excepticismo respecto de la afirmación de que pronto el Capital no
necesitará trabajadores y por consecuencia, provocará su propia
desaparición:

> "El Capital no puede caer en el olvido, pero tampoco puede ser
> engañado o maldecido para que desaparezca... La bibliografía del "fin
> del trabajo"...  crea una politíca fallida porque en definitiva trata
> de convencer tanto a amigos como a enemigos de que el capitalismo ha
> acabado a espaldas de todes.
 
Esta era una crítica de Jeremy Rifkin y Antonio Negri en los '90, pero
hoy podría también aplicarse al trabajo de Paul Mason, Snricek
y Williams. Hay algo mágico en dejar que la automatización haga todo el
trabajo anticapitalista por vos.  Desgraciadamente no hay ningún ardid
que acabe con el capitalismo. Incluso si sostienen en muchos puntos que
la automatización no es un fin técnico sino político,  están dejando que
en muchos aspectos la automatización maneje el volante de la
política. Ya he mencionado los peligros del economismo. Hoy algo nuevo
pareciera estar emergiendo, lo cual es aparentemente muy prevalente
entre "ecomodernistas" progresistas: tecnologismo. La creencia de que un
futuro bajo en carbono es solo posible incrementando la innovación y los
avances tecnológicos en vez de mediante una transformación a gran escala
de nuestras relaciones sociales y políticas. Snricek y Williams tratan
de eludir el tecnologismo, pero su extrema fascinación con la
automatización los acerca peligrosamente.

Tercero, incluso si la automatización estuviera en alza, soy escéptico
acerca de cómo podría limitar la expansión del capitalismo. Como lo
expresó Peter Linebaugh, les Luditas se oponían a la automatización, no
solamente porque les estaba costando sus propios trabajos, sino porque
sabían que la automatización de manufactura textil significaba la
esclavización y la atracción hacia el sistema capitalista de millones de
esclaves y personas pertenecientes a los pueblos originarios en las
colonias. La automatización, desde este punto de vista es un "problema"
local asumido desde una perspectiva miope del norte: no va a acabar con
la siempre creciente tala de bosques, confinamientos, destrucción de
medios de subsistencia, y la creación de clases itinerantes forzadas
a incorporarse a la economía extractivista. Independientemente de que la
automatización sea capitalista o comunista, si no es regularizada, da
lugar al incremento de conflictos medioambientales a nivel global. Pero
las crecientes tasas de extracción de recursos no son mencionadas como
un problema en el libro, ni tampoco proponen una alianza estratégica con
aquelles afectades por la industria extractivista.

Esto nos lleva a lo que es quizás la falta más frustrante en todo el
libro: sus muy débiles propuestas medioambientales.

Sorprendentemente, hay dos instancias en las que presentan maneras de
abordar el "problema medioambiental": durante su argumentación en favor
de la automatización también mencionan que una mayor eficiencia
minimizaría el uso de energía. En otro apartado, sugieren que el paso
a una semana laborable de cuatro días limitaría el uso de energía que se
consume en transporte.

Pero la eficienccia no funciona de esa manera. Si vas a extraer una
lección de la economía ecológica, es esta regla de oro que habrá que
repetir a cada tecno optimista que te cruces: *Sin limitar de alguna
manera el uso de recursos y energía (ej. a través de impuestos),
cualquier avance en eficiencia, probablemente conducirá de manera
progresiva a un mayor uso de recursos, no menos*.  Es el llamado efecto
rebote o Paradoja de Jevons.

Se deduce que **no hay garantía** de que acortar la semana laborable
sería más amigable para el medioambiente. Eficiencia y más tiempo libre
puede conducir con la misma facilidad a un daño ecológico mayor, no
menor. En cualquier régimen político (capitalista o comunista) en el que
existen insuficientes límites o regulaciones sobre el uso total que la
sociedad hace de energía y materiales, mientras que las ganancias de la
inversión son invertidas en más producción, los avances en eficiencia
*causarán un aumento exponencial en la producción de energía
y materiales.*

Discutiendo este tema con gente de la comunidad del decrecimiento,
Viviana Asara señala que esto no es solo un problema de justicia
ambiental, la cual sale perdiendo con el aumento de la producción, sino
también un problema de límites energéticos.

El concepto de TRE (tasa de retorno energético) demuestra que
a diferencia de los combustibles fósiles, la energía renovable tiene un
muy bajo retorno de la inversión. En favor del argumento, asumamos que
una economía de lujo totalmente automatizada tiene el mismo consumo de
energía que el de la economía de hoy, más eficiente pero produciendo más
cosas. Pero a causa del Tre extremadamente bajo de la energía renovable,
ese tipo de economía podría requerir la transformación de la superficie
total de la tierra en paneles solares, una visón del futuro no solo
infernal, sino imposible.

Podemos discutir extensamente acerca de si es efectivamente posible
producir la misma cantidad de energía utilizando solamente renovables,
pero el punto es que Srnicek y Williams no se avienen siquiera
a sostener esa discusión, algo que debería considerarse necesario si tu
propuesta es aumentar la actividad industrial global en tiempos de
cambio climático. Como me dijo Asara en un correo electrónico, "Su
utopía de automatización 'supuestamente sostenible' pasa por alto
cualquier idea de la realidad biofísica".

Aquí es donde los análisis aceleracionistas y decrecentistas más
difieren. El decrecimiento toma el "metabolismo" de la economía como una
cuestión clave; es decir, cuánta energía y material utiliza. Teniendo en
cuenta que la innovación habilita la aceleración de este metabolismo
y a causa de que este aumento en el metabolismo tiene impactos sociales
y ecológicos, frecuentemente descargados sobre gente que no se beneficia
de la tecnología, es necesario que la toma de decisiones con respecto
a los límites de la tecnología sea colectiva.

En este sentido, reapropiarse de la tecnología o hacerla más eficiente
no es suficiente. De hecho, sin transformar totalmente el modo en el que
el capitalismo reinvierte su plusvalía, requiriendo una transformación
fundamental en los sistemas financieros, desafortunadamente, la
automatización va a contribuir con la expansión del capitalismo, en vez
de permitirnos superarlo.

Si el capitalismo busca siempre colectivizar los impactos y privatizar
los beneficios, entonces el comunismo no debería ser la colectivización
de los beneficios y la externalización de los impactos hacia las
periferias o las futuras generaciones. Este es el peligro del "comunismo
de lujo totalmente automatizado". Estos peligros no son discutidos por
los textos aceleracionistas, pero debería serlo.

Quizás esta es la diferencia ideológica clave: les aceleracionistas
hacen un gesto tan extremadamente modernista que se niegan a limitar su
utopía, solo hay posibilidades. Contrariamente, el decrecimiento se basa
en politizar los límites que hasta ahora han sido dejados en manos de la
esfera privada. Esto podría querer decir, en palabras de un empleado de
Wall Street, "Preferiría no hacerlo" ("I would prefer not to") a ciertas
tecnologías.

## ¿Qué es velocidad?

Algo querrá decir cuando dos importantes segmentos de la izquierda
radical han gravitado sobre los términos de "decrecimiento"
y "aceleracionismo", tan opuestos como puedan ser.

En mi opinión, hay algo más bien nuevo aquí que nos lleva la discusión
más allá de campesines vs. trabajadores, localismo vs. la toma del
estado: la introducción de la cuestión de la velocidad dentro del
pensamiento de izquierda.

La abordan de maneras muy diferentes. Para el decrecimiento,
"crecimiento" es aceleración de los flujos energéticos y materiales del
sistema económico a un ritmo exponencial, tanto como la ideología que lo
justifica; llamémosle *velocidad sociometabólica*. Su proyecto político
entonces viene a desafiar frontalmente esa ideología, tanto como
a repensar la teoría económica en orden de permitir a las sociedades un
reaseguro del bienestar, pero también para transformar el modo en que
energía y material son utilizados; algo necesario para un sistema
económico más justo al concepto marxista de las condiciones materiales
de las relaciones humanas.

Les aceleracionistas por otro lado, piensan la velocidad de un modo
mucho más figurativo: se refieren a las condiciones materiales de las
relaciones humanas.  Para elles aceleración significa ir más allá de los
límites del capitalismo, lo que requiere un posicionamiento totalmente
moderno. Esta es una *velocidad sociopolítica*: el desplazamiento de las
relaciones sociales, como resultado de cambiar los sistemas
tecnológicos.

Ambas, pienso, han puesto su dedo sobre una cuestión crucial de nuestro
tiempo, pero en direcciones algo diferentes: ¿Puede lo que nos otorga la
modernidad, una colosal red infraestructural global de extracción,
transporte y fabricación, ser democratizada? Para el aceleracionismo
esto requeriría hacer que esa red sea más eficiente y modificar los
sistemas políticos para hacerlos más fáciles a la convivencia y cambiar
las relaciones sociales más allá del capitalismo. Para el decrecimiento
requeriría ralentizar ese sistema y desarrollar sistemas alternativos
fuera de él. No creo que estos dos objetivos sean mutuamente
excluyentes. Pero haría falta, por un lado, ir más allá de fórmulas
simplistas para el cambio del sistema, y por otro, posturas
antimodernas.

Pero también vale la pena ir un paso más allá y preguntarse si ese
sistema infraestructural tomaría amablemente este cambio de marchas o si
simplemente expulsaría al pasajero.

Para explorar esta cuestión es útil referirse a Paul Virilio, el
principal "filósofo de la velocidad". En _Velocidad y Política_, Virilio
describe cómo se provocaron cambios en las relaciones sociales en base
a un incremento de la velocidad de personas, máquinas y armas. Según
Virilio la historia de la larga emergencia de Europa, desde feudalismo
hacia la modernidad del siglo XX, fue una historia de crecimiento del
metabolismo de corporalidades y tecnologías. Cada régimen sucesivo
significó una recalibración de esta velocidad, acelerándola,
manejándola. Para Virilio los sistemas políticos, sean totalitarios,
comunistas, capitalistas o republicanos, emergieron como una respuesta
a los cambios producidos por esta fluctuación de la velocidad y a la
vez, como un modo de gestionar la coexistencia de lo humano y lo
tecnológico.

Lo importante para este debate es que Virilio no separa los dos tipos de
velocidad: el cambio en las relaciones sociales significa también el
cambio en el ritmo metabólico; son lo mismo y deben ser teorizados
simultáneamente.

Hacer esto puede ser útil tanto al decrecimiento como al
aceleracionismo. Mientras que el decrecimiento no tiene un análisis
sucinto de cómo responder a los cambios de los regímenes sociotécnicos
de hoy (un punto fuerte del aceleracionismo), al mismo tiempo, el
aceleracionismo no teoriza suficientemente sobre los flujos crecientes
de material y energía que resultan de este cambio de ritmo. Dicho de
otro modo, la eficiencia sola puede limitar sus efectos
desastrosos. Como las teóricas del decrecimiento lo han señalado, los
límites medioambientales deben ser politizados, por lo tanto, el control
sobre la tecnología debe ser democratizado y las velocidades metabólicas
deben ser desaceleradas para que la Tierra continúe siendo habitable.

Es necesario hacerse grandes preguntas, preguntas sin responder por la
exhortación simplista a "cambiarle la velocidad al capitalismo". Cuando
la velocidad se cambia, el problema de los límites metabólicos no será
resuelto través de la "eficiencia", debe reconocerse que mayor
eficiencia y automatización han conducido (y probablemente lo seguirán
haciendo) a un aumento del extractivismo y a una escalada global de las
injusticias medioambientales. U otra: ¿qué significa  aceleracionismo en
el contexto de una maquinaria de guerra que históricamente ha prosperado
a través de la velocidad, la logística y la conquista de la distancia?
¿es posible una aceleración no violenta, y qué aspecto tendría la lucha
de clases en ese escenario?

Para ser justo, el decrecimiento tampoco responde todas las grandes
preguntas. Ha habido poca discusión acerca de cómo la desaceleración de
masas sería posible cuando, como demuestra Virilio, el cambio de masas
ha ocurrido históricamente a través de la aceleración. ¿Puede
desacelerar la hegemonía?

Si el decrecimiento carece de una teoría robusta acerca de cómo generar
un cambio de régimen, entonces la marca aceleracionismo de Williams
y Srnicek no permite un vocabulario pluralista que mire más allá de su
limitada idea sobre lo que constituye un cambio de sistema. Y sin
embargo, los defensores de cada ideología se encontrarán probablemente
en la misma sala en las décadas por venir. A pesar de su "marca"
opuesta, quizás deberían conversar. Tienen mucho que aprender unes de
otres.


[^ayd-1]: Foro de la Sociedad Futura.

[^ayd-2]: Figurativo: tormenta de ideas. Técnica para generar ideas en forma grupal.

[^ayd-3]: Pequeñas hojas de papel autoadhesivo de varias dimensiones, formas y colores.

[^ayd-4]: Juego de palabras entre "ahora" y "utopías".

[^ayd-5]: Término acuñado por Srnicek y Williams, que podría traducirse como "política popular".

[^ayd-6]: _Think tank_ podría traducirse como "laboratorio de ideas",
  "instituto de investigación", "gabinete estratégico de expertos",
  "centro de pensamiento" o "centro de reflexión". Es una institución
  o grupo de expertos, generalmente vinculados al poder, cuya función
  principal es generar consensos en torno a política social, estrategia
  política, economía, asuntos militares, tecnología, cultura,
  etc. Suelen estar vinculados con laboratorios militares, empresas
  privadas, instituciones académicas, partidos políticos, grupos de
  presión y lobby.  Su objetivo suele ser el de ejercer influencia sobre
  los gobiernos y otros sectores del campo del poder para incidir en la
  opinión y las políticas públicas.

