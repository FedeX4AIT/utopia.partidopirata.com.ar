---
title: "Guía para refutar mitos"
author: John Cook
layout: post
---

# Guía para refutar mitos

Refutar un mito es problemático.  A menos que se tenga mucho cuidado, el
esfuerzo por refutar desinformación puede reforzar inadvertidamente el
mismo mito que se quiere combatir.  Para evitar estos "efectos
paradójicos", una refutación efectiva necesita tres elementos
principales.  Primero, la refutación debe centrarse en los hechos
principales antes que en el mito, para no popularizarlo.  Segundo,
cualquier mención del mito debe estar precedida por advertencias
explícitas de que tal información es falsa.  Finalmente, la refutación
debe incluir una explicación alternativa que dé cuenta de los problemas
contenidos en el mito original.

## Refutando el primer mito sobre la refutación

Es evidente que las sociedades democráticas deben basar sus decisiones
en información precisa.  Sin embargo, existen muchos temas en los que
pueden encontrarse segmentos de la comunidad profundamente
desinformados, especialmente cuando hay intereses particulares
involucrados[^1][^2].  Reducir la influencia de esta desinformación es un
desafío difícil y complejo.

Un error común es la noción de que eliminar la influencia de un mito es
tan simple como insertar más información en la mente de las personas.
Este enfoque asume que la desinformación se debe a la falta de
conocimiento y que la solución es más información (en divulgación
científica, es conocido como el "modelo deficitario").  Pero ese modelo
es falso: la gente no procesa información como un disco duro descarga
datos.

Refutar desinformación involucra tratar con procesos cognitivos
complejos.  Para transmitir conocimiento exitosamente, hace falta
entender cómo las personas procesan información, cómo modifican su
conocimiento previo y cómo sus cosmovisiones afectan su habilidad para
pensar racionalmente.  No solo importa qué piensan las personas, sino
también cómo lo hacen.

Primero, aclaremos qué queremos decir con el término "desinformación":
lo usamos para referirnos a cualquier información adquirida que resulta
ser incorrecta, sin importar cómo ni por qué lo fue en un principio.  Nos
importan los procesos cognitivos que determinan cómo se procesan
correcciones a la información adquirida; si encontrás que algo que creés
es falso, ¿cómo actualizás tu conocimiento?

Una vez que se recibe desinformación, es muy difícil eliminar su
influencia.  Esto fue demostrado en un experimento de 1994 en el que se
expuso a les sujetes a desinformación sobre un incendio ficticio en un
depósito y luego se les dio una corrección que aclaraba las partes
incorrectas de la historia[^3].  A pesar de recordar y aceptar la
corrección, les sujetes mostraron un efecto de persistencia,
refiriéndose a la desinformación a la hora de responder preguntas sobre
la historia.

¿Es posible eliminar por completo la influencia de la desinformación?  La
evidencia indica que sin importar cuán vigorosa y repetidamente la
corrijamos (por ejemplo, repitiendo la corrección una y otra vez) su
influencia se mantiene detectable[^4].  El viejo refrán es cierto:
hierba mala nunca muere.

Hay también una complicación adicional.  No solo es difícil eliminar la
desinformación, sino que tratar de refutar un mito puede terminar
reforzándolo.  Se han observado varios "efectos paradójicos" que surgen
de popularizar el mito[^5][^6], de proveer demasiados argumentos[^7], o
de proveer evidencia que amenaza una cosmovisión personal[^8].

Lo último que querés hacer al refutar desinformación es meter la pata y
empeorar las cosas.  Por lo tanto esta guía tiene un objetivo
específico: proveer consejos prácticos para refutar desinformación
exitosamente y evitar los varios efectos paradójicos.  Para lograrlo, es
necesario conocer los procesos cognitivos relevantes.  Explicamos parte
de la interesante investigación psicológica en el área y terminamos con
un ejemplo de refutación efectiva de un mito común.

## El efecto paradojal por familiaridad

Para refutar un mito, en general hay que mencionarlo; de otra manera,
¿cómo sabrá la gente de qué estás hablando?  Sin embargo, esto
familiariza a la gente con el mito y, por lo tanto, facilita que lo
acepten como cierto.  ¿Significa esto que refutar un mito podría
reforzarlo en la mente de las personas?

Para probar este efecto paradojal, se les mostró a unes sujetes un
folleto que refutaba algunos mitos comunes sobre las vacunas contra la
gripe.  Luego se les pidió que separaran los mitos de los hechos.
Cuando se les preguntó inmediatamente luego de leer el folleto si
lograron identificar los mitos satisfactoriamente.  Sin embargo, cuando
se les preguntó 30 minutos después de leerlo, algunes puntuaron peor que
antes de la lectura.  La refutación reforzó los mitos.

El efecto paradojal es real.  El mecanismo involucrado es el hecho de
que la familiaridad aumenta las probabilidades de aceptar la información
como cierta.  Inmediatamente después de leer el folleto recordaron los
detalles que refutaban el mito e identificaron correctamente los mitos.
Pero al pasar el tiempo la memoria de los detalles se desvaneció y todo
lo que recordaron fue el mito sin la "etiqueta" que lo identificaba como
falso.  Este efecto es particularmente fuerte en adultes mayores debido
a que son más proclives a olvidar los detalles.

A veces no mencionar el mito no es una alternativa práctica.  En ese
caso, el énfasis de la refutación debe estar en los hechos.  La técnica
habitual de titular la refutación con una fuente grande y en negrita es
lo último que querés hacer.  En vez de eso, comunica el hecho central en
el título.  Tu refutación debe comenzar con el énfasis en los hechos, no
el mito.  Tu objetivo es aumentar la familiaridad de los hechos.

## El efecto paradojal por exceso

Un principio al que les divulgadores científiques muchas veces no se
apegan es el de hacer que el contenido sea fácil de procesar.  Esto
significa que sea fácil de leer, fácil de entender y conciso.  La
información que es fácil de procesar es más probable que sea aceptada
como cierta.  Solo incrementar el contraste del texto para que sea más
legible, por ejemplo, puede aumentar la aceptación de una
afirmación[^9].

La sabiduría popular sugiere que se es más exitose al refutar un mito
cuantos más contra-argumentos se provean.  Pero resulta que lo contrario
puede ser cierto.  Cuando se trata de desmentir desinformación, menos
puede ser más.  Las refutaciones de tres argumentos, por ejemplo, fueron
mejores para reducir la influencia de la desinformación al compararlas
con refutaciones de doce argumentos, las que terminaron reforzando el
mito.

El efecto paradojal por exceso ocurre porque procesar muchos argumentos
requiere más esfuerzo que solo considerar unos pocos.  Un mito simple es
cognitivamente más atractivo que una corrección excesivamente
complicada.

La solución es mantener tu contenido simple y fácil de leer.  Para hacer
tu contenido simple de procesar usá todas las herramientas disponibles.
Usá lenguaje simple, oraciones cortas, subtítulos y párrafos.  Evitá
lenguaje dramático y comentarios despectivos que alejen a las personas.
Atenete a los hechos.  Terminá en un mensaje fuerte y simple que la
gente pueda recordar y twittear a sus amigues, como "97 de cada 100
climatólogos sostienen que las humanas están causando el calentamiento
global"; o "Estudio muestra que la vacuna triple viral es segura".  Usá
gráficos para ilustrar tus puntos cuando sea posible.

Los científicos hace tiempo que siguen los principios del modelo
deficitario, que sugiere que la gente sostiene nociones erróneas porque
no tiene toda la información.  Pero demasiada información puede ser
contraproducente.  Mejor seguir el principio KISS: Keep It Simple,
Stupid!  (¡Hazlo Simple, Estúpide!)

## El efecto paradojal por cosmovisión

El tercero y quizás más potente efecto paradojal ocurre en temas que
afectan la cosmovisión y sentimiento de identidad cultural de las
personas.  Existen varios procesos cognitivos que pueden causar,
inconscientemente, un procesamiento sesgado de la información.  Para
aquelles muy aferrades a sus creencias, encontrar contra-argumentos
puede reforzar sus puntos de vista.

Un proceso cognitivo que contribuye a este efecto es el sesgo de
confirmación, por el cual se busca solo la información que concuerda con
las creencias previas.  En un experimento, a les sujetes se les dio
información sobre problemáticas controversiales como el control de armas
o la acción afirmativa.  Cada texto fue etiquetado según su fuente,
indicando claramente si argumentaría a favor o en contra (por ejemplo,
Asociación Nacional del Rifle vs.  Ciudadanes en Contra de las Armas).
Aunque se les instruyó ser imparciales, les sujetes optaron por las
fuentes que concordaban con sus creencias preexistentes.  El estudio
descubrió que cuando se les presenta un conjunto de hechos equilibrados,
las personas refuerzan sus creencias previas decantándose hacia la
información con la cual concuerdan.  La polarización fue mayor entre
quienes sostenían sus creencias fervientemente[^10].

¿Qué pasa cuando se quita la posibilidad de elección y se presentan
argumentos contrarios a la propia cosmovisión?  En este caso el proceso
cognitivo que entra en juego es el sesgo de desconfirmación, la otra
cara del sesgo de confirmación, por el que se gasta mucho más tiempo y
pensamiento argumentando contra argumentos opuestos.

Esto se demostró cuando a republicanos que creían que Saddam Hussein
tenía vínculos con el ataque del 11 de septiembre se les mostró
evidencia de que no estaban relacionados, incluyendo una cita textual
del presidente George Bush[^11].  Solo el 2% de los participantes
cambiaron de opinión (curiosamente, un 14% negó haberlo creído en primer
lugar).  La gran mayoría se aferró al vínculo entre Iraq y el 11 de
septiembre utilizando una gran cantidad de argumentos para hacer a un
lado la evidencia.  La respuesta más común fue la de poner sobre la mesa
hechos confirmatorios mientras se ignoraba la información
contradictoria.  El proceso de hacer hincapié en los hechos
confirmatorios resultó en un refuerzo de las creencias erróneas.

Si los hechos no pueden librar a una persona de sus creencias
preexistentes (y a veces pueden empeorar las cosas), ¿cómo podemos
reducir el efecto de la desinformación?  Existen dos razones para
mantener la esperanza.

Primero, el efecto paradojal por cosmovisión es más fuerte entre
aquelles que ya están muy aferrades a sus creencias.  Por lo tanto,
tenés mejores posibilidades de corregir la desinformación entre quienes
todavía no tienen una opinión sólida.  Esto sugiere que la difusión
debería estar dirigida hacia la mayoría indecisa antes que a la minoría
anquilosada.

Segundo, se pueden presentar mensajes en formas que minimizan la usual
resistencia psicológica.  Por ejemplo, cuando los mensajes que
amenazaban la cosmovisión fueron acompañados por autoafirmación, los
sujetos fueron más imparciales al considerar la información a favor y en
contra[^12][^13].

La auto-afirmación puede conseguirse pidiendo a la gente que escriba
unas oraciones sobre algún momento en el cual se sintieron bien con
elles mismes porque actuaron según un valor importante para elles.  Así,
las personas se vuelven más receptivas a mensajes que de otra forma
amenazarían su cosmovisión, comparados con quienes no reciben
auto-afirmación.  Es interesante que este "efecto de autoafirmación" es
más fuerte entre aquelles cuya ideología es central para su sentido del
propio valor.

Otra forma de hacer la información más aceptable es "encuadrándola" de
forma tal que sea menos amenazante.  Por ejemplo, es mucho más probable
que un neo-liberal acepte una tarifa si se la llama "compensación por
carbono" que "impuesto al carbono", mientras que en les progresistas o
zurdes no se observa tal efecto, pues sus valores no se ven amenazados
por la palabra "impuesto"[^14].  Con la auto-afirmación y el encuadre no
se trata de manipular a las personas.  Se trata de darle una oportunidad
a los hechos.

## Rellenar el hueco con una explicación alternativa

Asumiendo que has logrado evitar los varios efectos paradojales, ¿cuál
es la forma más efectiva de refutar un mito?  El desafío es que una vez
que la desinformación entra en la mente es muy difícil de eliminar,
incluso cuando la corrección es aceptada y recordada.
