# Utopías
Este repositorio contiene las fuentes en MarkDown de todos los textos producitos de la editorial Utopía Pirata y permite generar automáticamente el sitio web con el catálogo, además de las versiones ePub, PDF, HTML, MediaWiki y LaTeX de cada uno de ellos.

# Cómo instalar este repo

1. Configurar un entorno Ruby 2.6.5 como se indica aquí https://docutopia.tupale.co/ruby:configurar_entorno_de_trabajo
2. Instalar pandoc version 2.9 o posterior. En algunas distros la versión incluída es anterior. Instrucciones aquí: https://pandoc.org/installing.html#linux
    1. Verifica que tu versión de pandoc es posterior a la 2.9 con `pandoc -v`.
3. Instalar tectonic como se indica aquí: https://tectonic-typesetting.github.io/en-US/install.html
4. clonar el repo: `git clone https://0xacab.org/utopias-piratas/utopia.partidopirata.com.ar`
5. `cd utopia.partidopirata.com.ar`
6. Copiar las fuentes `cp _fonts/* ~/.fonts/`
7. Intalar requerimientos con  `bundle`

# Cómo compilar el sitio
Compilar el sitio general el catálogo y todas las versiones de los libros.

Desde la carpeta del repositorio ejecutar `bundle exec jekyll build`

Este proceso toma varios minutos, dependiendo de la velocidad del equipo. Además la primera vez que corre descarga algunos paquetes de LaTeX.

https://www.partidopirata.com.ar