---
title: "#LetsApostatize from mass-surveillance asocial networks"
author: Partido Interdimensional Pirata
layout: post
cover: assets/covers/apostatemos_de_las_redes_asociales.png
---

> **To apostatize** is to renounce a (religious, political, etc.)
> belief.  Collective Apostasy[^apostasy] is a grassroots campaign
> directed to people who were baptized at born so they can collectively
> renounce to this sacrament.  The objective is to stop being a
> statistic that the catholic church uses to interfere in public policy.
> Thanks to the baptism registry, the catholic church considers that 90%
> of Argentinians are catholic and for that they receive public funding,
> estimated at $ 13.000.000.000 ARS[^ars] per year.

[^apostasy]: To know more about Collective Apostasy:
  <http://apostasia.com.ar>

[^ars]: Nearly $ 350.000.000 USD at the time of writing.

To apostatize from asocial networks of mass-surveillance doesn't mean to
abandon every form of gathering, keeping ourselves informed, sharing or
having interesting discussions.  There're other social networks that
are libre and communitary.

We propose several strategies and scenarios.  If you were thinking about
leaving Facebook (and Twitter, and Google, etc.), keep reading :)

# Drastic

If you're fed up and you don't want to know anything else, you can close
your accounts and come with us to the Fediverse.

The Fediverse is a network of interconnected communication nodes, that
is, they federate.  To talk with someone who's on Facebook, you need a
Facebook account, and the same for Twitter, and for every other
platform.  In the Fediverse you just need to pick one of the many nodes,
the one that looks more friendly, more trustable, and through it we can
talk with every other.

Networks like Diaspora*, GNU Social, Mastodon, Friendica, Hubzilla, and
others, are different software that allow any community to
self-administer a node.  If before this hability belonged only to
corporations, now every collective can have it's own network and
federate it with the rest.

# Alternative media

While we always propose to abandon capitalist networks, we also
understand that they're currently the most massive platforms for public
opinion.  This turns them into spaces for struggle.

The strategy we have in these cases is to keep these networks, but at
the same time, also publish in free and communitary networks.  In this
way, people that are migrating to them can keep up with the news.  It
also prevents the effect of joining a new space but abandoning it
immediately because "there's no one to talk to".


# For organizations

We've found that some collectives are using Facebook not only for public
communications, but also for self-organization and decision-making.  As
Audre Lorde said "the master's tools will never dismantle the master's
house!"  This means that even as Facebook provides groups, in its social
model there's no room for horizontal organization.  To Facebook, we're
only individuals shouting opinions at each other.

There're other tools crafted for horizontal organization and consensus
decision-making, like [Loomio](https://loomio.org).  Loomio has been
inspired by Occupy and it happens to be free software also.  At Partido
Interdimensional Pirata we've used it and we invite every collective to
use it.

# Regular uses

Sometimes we want to remove ourselves from capitalist network, but we
can't because we'll left behind our affects, families, works, studies...

For these cases, we propose using alternative apps, so our devices won't
betray us by giving away information we aren't publishing by ourselves.
We can still participate, but more than less in our terms, instead of
the ones imposed by corporations.

_Tinfoil for Facebook_ or _Twidere_, by instance, are free software apps
that allow us to access these capitalist networks without the official,
gigantic and treacherous apps.


# Mutual aid

As any apostasy must be collective to be effective, we propose to gather
and help each other to abandon capitalist networks, and flee to libre
and communitary ones to keep finding ourselves in the cyberspace,
with no exploitation, with no manipulation, with no harrassment.

Let's apostatize together!

# Disclaimer

There're other alternatives that are distributed, anonymous and
encrypted.  The apps and nodes we can recommend here are only few,
selected with a criteria of security and freedom for the people.
Nevertheless, a communication media for left-libertarian organization
of actions sistematically suppresed by the state and its institutions is
only possible with a little more effort.  Do you think your affinity
group, collective, party or movement will want to have and use better
technologies for communication?

You can contact us... there's no interdimentional magic, but there's
much to learn, teach, and even develop.

PARTIDO INTERDIMENSIONAL PIRATA

Mail: contacto@partidopirata.com.ar

Diáspora*: partidopirataargentina@diaspora.com.ar

Mastodon: @pip@todon.nl

We're also in the process of apostatizing P)

Twitter: @PartidoPirataAr

Facebook: Partido Pirata Argentino
