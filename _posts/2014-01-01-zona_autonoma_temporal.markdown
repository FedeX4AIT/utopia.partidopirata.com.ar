---
title: "Zona Autónoma Temporal"
author: Hakim Bey
layout: post
cover: "assets/covers/single/zona_autonoma_temporal.png"
slider: "assets/covers/slider/zona_autonoma_temporal.png"
papersize: a5paper
toc: true
---

Zona Autónoma Temporal
======================

Hemos decidido despublicar este texto porque descubrimos que Hakim Bey
hace apología de la pedofilia...

[Leaving out the ugly part - Hakim Bey/Peter Lamborn Wilson](https://libcom.org/library/leaving-out-ugly-part-hakim-bey)

Entendiendo que es un texto fundante del ciberespacio, tenemos ganas de
re-escribir las ZATs con nuestras perspectivas.  Te invitamos a sumarte
:)

<contacto@partidopirata.com.ar>
